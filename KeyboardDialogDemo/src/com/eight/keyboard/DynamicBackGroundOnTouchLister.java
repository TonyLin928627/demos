package com.eight.keyboard;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;


public abstract class DynamicBackGroundOnTouchLister implements OnTouchListener {
	
	private int upImageResourceId;
	private int downImageResourceId;

	public DynamicBackGroundOnTouchLister(int upImageResourceId, int downImageResourceId){
		this.upImageResourceId = upImageResourceId;
		this.downImageResourceId = downImageResourceId;
	}
	
	public abstract void doJob(View view);
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		boolean rtn = true;
		switch (event.getAction()){
		case (MotionEvent.ACTION_DOWN):
			
			v.setBackgroundResource(this.downImageResourceId);
			
			//Log.d("DynamicBackGroundOnTouchLister", v.getId()+": down");
		
			rtn = false;
			break;
			
		case (MotionEvent.ACTION_UP):
			
			v.setBackgroundResource(this.upImageResourceId);
			
			//Log.d("DynamicBackGroundOnTouchLister", v.getId()+": up");
			
			if(event.getX()>=0 && event.getX()<=v.getWidth() && event.getY()>=0 && event.getY()<=v.getHeight()){
				//Log.d("DynamicBackGroundOnTouchLister", v.getId()+": doJob");
				this.doJob(v);
			}
				
			
			break;
			
		case (MotionEvent.ACTION_MOVE):
			
			if(event.getX()<0 || event.getX()>v.getWidth() ||event.getY()<0 || event.getY()>v.getHeight()){
				v.setBackgroundResource(this.upImageResourceId);
				//Log.d("DynamicBackGroundOnTouchLister", v.getId()+": move out");
				
			}else{
				v.setBackgroundResource(this.downImageResourceId);
		
				//Log.d("DynamicBackGroundOnTouchLister", v.getId()+": move in");
			}
		
			break;
		}
	
		return rtn;
	}

}
