package com.eight.keyboard;



import java.util.ArrayList;
import java.util.List;

import com.example.keyboarddialogdemo.MainActivity;
import com.example.keyboarddialogdemo.R;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


/**
 * @author Tony Lin
 * 
 * This is a Business-Logic-free keyboard that can be invoked anywhere in the app.
 * ToDo : Unit test. 
 *
 */
public class KeyboardDialog extends AlertDialog {
	
	//the TextView on the top of keyboard showing the current holding string.
	private TextView previewTv;
	
	//the view object that is bounded with the keyboard, it is an instance of EditText, Button or TextView.
	private View objectView;
	
	//the Button at the left-top of keyboard, the keyboard is closed when this button is pressed.
	private Button hideBtn;
	
	//flag that indicates if the keyboard is currently in number mode.
	private boolean isInNumber = false;
	
	//flag that indicates if the keyboard is currently in upper case or lower case mode.
	private boolean isInUpperCase = true;
	
	//a list that contains all the alphabetic keys. 
	private List<Key> keyList;
	
	//the ImageButton that represent back button on the keyboard;
	private ImageButton keypadBackIb;
	
	//the ImageButton that represents back button on the keyboard;
	private Button keypadCaseBtn; //keypadZ, keypadX, keypadC, keypadV, keypadB, keypadN, keypadM; 
	
	//the Buttons that represents Number, Space and Enter keys on keyboard.
	private Button keypadNumberBtn, keypadSpaceBtn, keypadEnterBtn;
	
	//The maximum length that the holding string could be. Default is 32 if has not been set explicitly.
	private final int MAX_CONTENT_LENGTH;
	
	//refer to onContentChangedListener, could be null if has not been set explicitly.
	private OnContentChangedListener onContentChangedListener;
	
	//refer to onKeyboardExitListener, could be null if has not been set explicitly.
	private OnKeyboardExitListener onKeyboardExitListener;
	
	//the original content of the objectView. this is used to restore the content of bounded view when hide button is press.
	private String originConent;

	//A stringBuffer that holds the holding string, its value was initialized with originContent. and will be shown by previewTv 
	private StringBuffer stringHolding;

	//This field should be set globally. 
	public static Typeface ligTf = MainActivity.ligTf;

	
	/**The trigger method to activate a keyboard dialog.
	 * 
	 * @param context The context that is passed in from the invoker.
	 * @param objectView  The bounded view, the keyboard will be editing the content of this view.
	 * @param maxContentLenght The maximum content length that the key will handle.
	 * @param l1 OnKeyboardExitListener, could be null.
	 * @param l2 OnContentChangedListener, could be null.
	 * @param screenSize The passed in screen size determines the left margin of the keyboard position, this parameter is used to adapt different orientation.
	 *
	 * 		if (screenSize.x > screenSize.y) {
	 *			wlp.x = 360;
	 *		} else{
	 *			wlp.x = 0;
	 *		}
	 */
	public static void activateKeyboard(final Context context, final View objectView, int maxContentLenght, OnKeyboardExitListener l1, OnContentChangedListener l2, int leftMargin) {

		KeyboardDialog dialog = new KeyboardDialog(context,  objectView,  maxContentLenght, l1,  l2);

		Window window = dialog.getWindow();

		WindowManager.LayoutParams wlp = window.getAttributes();

		wlp.gravity = Gravity.BOTTOM | Gravity.LEFT;

		
		wlp.x = leftMargin;


		window.setAttributes(wlp);
		window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		dialog.show();

	}

	
	/** The trigger method to activate a keyboard dialog. with 32 as the default maxContentLenght.
	 * @param context
	 * @param objectView
	 * @param l1
	 * @param l2
	 * @param screenSize
	 */
	public static void activateKeyboard(final Context context, final View objectView, OnKeyboardExitListener l1, OnContentChangedListener l2, int leftMargin) {
		activateKeyboard(context,  objectView, 32, l1,  l2, leftMargin);

	}


	/** Private constructor. see activateKeyboard for the parameter definitions.
	 * 
	 * @param context
	 * @param objectView
	 * @param maxContentLenghth
	 * @param l1
	 * @param l2
	 */
	private KeyboardDialog(Context context,  View objectView, int maxContentLength, OnKeyboardExitListener l1, OnContentChangedListener l2) {
	//	this( context, R.style.keyboardDialog, maxContentLenghth, l1,  l2);
		super(context);
		this.onKeyboardExitListener = l1;
		this.onContentChangedListener = l2;
		this.MAX_CONTENT_LENGTH = maxContentLength;
		
		this.objectView = objectView;

		if(this.objectView instanceof EditText){
			this.stringHolding = new StringBuffer(((EditText)this.objectView).getText());

		}else if(this.objectView instanceof TextView){

			this.stringHolding = new StringBuffer(((TextView)this.objectView).getText());

		}else if(this.objectView instanceof Button){

			this.stringHolding = new StringBuffer(((Button)this.objectView).getText());
		}else{
			this.stringHolding = new StringBuffer();
		}

		this.originConent = new String(this.stringHolding.toString());
	}

	
	/**Create the objects for all the alphabetic keys. and add them to a list.  
	 */
	private void initKeys() {
		this.keyList = new ArrayList<Key>();

		this.keyList.add(new Key(this.findViewById(R.id.keypadA), 'A', 'a', '-'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadB), 'B', 'b', '/'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadC), 'C', 'c', '%'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadD), 'D', 'd', ':'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadE), 'E', 'e', '3'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadF), 'F', 'f', ';'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadG), 'G', 'g', '('));
		this.keyList.add(new Key(this.findViewById(R.id.keypadH), 'H', 'h', ')'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadI), 'I', 'i', '8'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadJ), 'J', 'j', '&'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadK), 'K', 'k', '@'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadL), 'L', 'l', '$'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadM), 'M', 'm', '.'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadN), 'N', 'n', ','));
		this.keyList.add(new Key(this.findViewById(R.id.keypadO), 'O', 'o', '9'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadP), 'P', 'p', '0'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadQ), 'Q', 'q', '1'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadR), 'R', 'r', '4'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadS), 'S', 's', '?'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadT), 'T', 't', '5'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadU), 'U', 'u', '7'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadV), 'V', 'v', '!'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadW), 'W', 'w', '2'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadX), 'X', 'x', '\''));
		this.keyList.add(new Key(this.findViewById(R.id.keypadY), 'Y', 'y', '6'));
		this.keyList.add(new Key(this.findViewById(R.id.keypadZ), 'Z', 'z', '+'));
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.keyboard);

		//set up previewTv
		previewTv = (TextView) this.findViewById(R.id.previewText);
		previewTv.setText(this.stringHolding.toString());

		//set up hideBtn
		this.hideBtn = (Button)this.findViewById(R.id.hide);
		this.hideBtn.setBackgroundColor(this.getContext().getResources().getColor(R.color.keypad_white));
		//add onTouchListener to hideBtn.
		this.hideBtn.setOnTouchListener(new DynamicBackGroundOnTouchLister(R.drawable.custom_keyboard_key_unpressed, R.drawable.custom_keyboard_key_pressed) {

			@Override
			public void doJob(View v) {

				stringHolding = new StringBuffer(originConent); 
				updateContent();

				KeyboardDialog.this.dismiss();
			}});

		//initialize alphabetic keys.
		this.initKeys(); 
		//loop and add OnTouchListeners to the alphabetic keys
		for (Key key : this.keyList) {  	
			key.getKeyBtn().setOnTouchListener(new DynamicBackGroundOnTouchLister(R.drawable.custom_keyboard_key_unpressed, R.drawable.custom_keyboard_key_pressed){

				@Override
				public void doJob(View v) {				

					if(KeyboardDialog.this.stringHolding.length() >= KeyboardDialog.this.MAX_CONTENT_LENGTH) return;

					Button btn = (Button)v;

					KeyboardDialog.this.stringHolding.append(btn.getText());

					updateContent();

					if(isInUpperCase && !isInNumber){
						isInUpperCase = false;
						for (Key key : keyList) {
							key.keyBtn.setText(String.valueOf(key.getLowerChar()));
						}

						keypadCaseBtn.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.keyboard_case_arrow_up , 0, 0);
					}

				}                

			});
		}

		//set up keypadNumberBtn
		this.keypadNumberBtn = (Button) this.findViewById(R.id.keypadNumber);
		this.keypadNumberBtn.setText("123"); 
		this.keypadNumberBtn.setTextSize(40);
		this.keypadNumberBtn.setTypeface(KeyboardDialog.ligTf);
		//add OnTouchListener to keypadNumberBtn
		this.keypadNumberBtn.setOnTouchListener(new DynamicBackGroundOnTouchLister(R.drawable.custom_keyboard_key_unpressed, R.drawable.custom_keyboard_key_pressed){

			@Override
			public void doJob(View v) {		
				isInNumber = !isInNumber;

				if (isInNumber) {
					for (Key key : keyList) {
						key.getKeyBtn().setText(String.valueOf(key.getNumberChar()));
					}
					keypadNumberBtn.setText( isInUpperCase ? "ABC" : "abc");
				} else {
					for (Key key : keyList) {
						key.getKeyBtn().setText(isInUpperCase ? String.valueOf(key.getUpperChar()) : String.valueOf(key.getLowerChar()));
					}
					keypadNumberBtn.setText("123");
				}
			}
		});

		//set up keypadCaseBtn
		keypadCaseBtn = (Button) this.findViewById(R.id.keypadCase);
		keypadCaseBtn.setText("case");
		keypadCaseBtn.setTextSize(38);
		keypadCaseBtn.setTypeface(KeyboardDialog.ligTf);
		//add OnTouchListener to keypadCaseBtn.
		keypadCaseBtn.setOnTouchListener(new DynamicBackGroundOnTouchLister(R.drawable.custom_keyboard_key_unpressed, R.drawable.custom_keyboard_key_pressed){

			@Override
			public void doJob(View v) {		
				isInNumber = false;

				keypadCaseBtn.setText("123");

				isInUpperCase = !isInUpperCase;

				Button btn = (Button)v;
				if (isInUpperCase) {
					btn.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.keyboard_case_arrow_down , 0, 0);

					for (Key key : keyList) {
						key.keyBtn.setText(String.valueOf(key.getUpperChar()));
					}
				} else {
					btn.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.keyboard_case_arrow_up , 0, 0);

					for (Key key : keyList) {
						key.keyBtn.setText(String.valueOf(key.getLowerChar()));
					}
				}                    

			}
		});

		//set up keypadSpaceBtn
		keypadSpaceBtn = (Button) this.findViewById(R.id.keypadSpace);
		keypadSpaceBtn.setText("space");
		keypadSpaceBtn.setTextSize(40);
		keypadSpaceBtn.setTypeface(ligTf);
		//add OnTouchListener to keypadSpaceBtn.
		keypadSpaceBtn.setOnTouchListener(new DynamicBackGroundOnTouchLister(R.drawable.custom_keyboard_key_unpressed, R.drawable.custom_keyboard_key_pressed){

			@Override
			public void doJob(View v) {
				KeyboardDialog.this.stringHolding.append(' ');

				updateContent();
			}
		});

		//set up keypadEnterBtn
		keypadEnterBtn = (Button) this.findViewById(R.id.keypadEnter);
		keypadEnterBtn.setText("enter");
		keypadEnterBtn.setTextSize(44);
		keypadEnterBtn.setTypeface(KeyboardDialog.ligTf);
		keypadEnterBtn.setTextColor(this.getContext().getResources().getColor(R.color.title_blue));
		//add OnTouchListener to setOnTouchListener.
		keypadEnterBtn.setOnTouchListener(new DynamicBackGroundOnTouchLister(R.drawable.custom_keyboard_key_unpressed, R.drawable.custom_keyboard_key_pressed){
			@Override
			public void doJob(View v) {
				KeyboardDialog.this.dismiss();
			}
		});

		//set up keypadBackIb
		keypadBackIb = (ImageButton) this.findViewById(R.id.keypadBack);
		//add OnTouchListener to keypadBackIb.
		keypadBackIb.setOnTouchListener(new DynamicBackGroundOnTouchLister(R.drawable.custom_keyboard_key_unpressed, R.drawable.custom_keyboard_key_pressed){

			@Override
			public void doJob(View v) {
				StringBuffer sb = KeyboardDialog.this.stringHolding;
				//delete the last char from the holding string.
				if (sb.length() > 0) {
					sb.deleteCharAt(sb.length() - 1);
					updateContent();
					
				//	v.setTag(System.currentTimeMillis());

				}
			}
		});
	}


	@Override
	protected void onStop() {
		
		//invoke call back method from onKeyboardExitListener if it has been set.
		if (this.onKeyboardExitListener!=null){
			this.onKeyboardExitListener.onKeyboardExit(this.stringHolding.toString(), this.objectView);
		}
		
		super.onStop();
	}

	
	/**Show the current holding string to the bounded objectView;
	 */
	private void updateContent(){
		if(this.objectView instanceof EditText){

			((EditText)this.objectView).setText(this.stringHolding.toString());

		}else if(this.objectView instanceof TextView){

			((TextView)this.objectView).setText(this.stringHolding.toString());

		}else if(this.objectView instanceof Button){

			((Button)this.objectView).setText(this.stringHolding.toString());
		}

		previewTv.setText(KeyboardDialog.this.stringHolding.toString());

		if (KeyboardDialog.this.onContentChangedListener !=null){
			KeyboardDialog.this.onContentChangedListener.onContentChanged(KeyboardDialog.this.stringHolding.toString(), this.objectView);
		}    

	}

	
	/**
	 * The internal class that represents the alphabet keys on the the keyboard.
	 *
	 */
	private class Key{
		private Button keyBtn;
		private char lowerChar;
		private char numberChar;
		private char upperChar;

		/**
		 * @param rawView The view from layout file, could be a button or imageView.
		 * @param upperChar The upper case letter that this key represents 
		 * @param lowerChar The lower case letter that this key represents 
		 * @param numberChar The number that this key represents 
		 */
		public Key(View rawView, char upperChar, char lowerChar,char numberChar) {
			super();

			this.upperChar = upperChar;
			this.lowerChar = lowerChar;
			this.numberChar = numberChar;


			this.keyBtn = (Button)rawView;
			this.keyBtn.setText(String.valueOf(this.upperChar));
			this.keyBtn.setTypeface(KeyboardDialog.ligTf);

		}

		/**
		 * @return the keyBtn
		 */
		protected Button getKeyBtn() {
			return keyBtn;
		}

		/**
		 * @return the lowerChar
		 */
		protected char getLowerChar() {
			return lowerChar;
		}

		/**
		 * @return the numberChar
		 */
		protected char getNumberChar() {
			return numberChar;
		}

		/**
		 * @return the upperChar
		 */
		protected char getUpperChar() {
			return upperChar;
		}


	}

	
	/**
	 * The listener interface that provides a call back to the keyboard invoker.
	 */
	public static interface OnContentChangedListener{
		/**
		 * The call back method, it is called every time when the current content changes by keyboard.
		 * @param stringHolding The holding string. which is a string that keyboard is holding.
		 * @param view The view component that is bounded with the keyboard. e.g. a EditText or Button. 
		 */
		public void onContentChanged(String stringHolding, View view);
	}

	/**
	 * The listener interface that listening the exit of the keyboard.
	 */
	public static interface OnKeyboardExitListener{
		/**
		 * @param stringHolding The holding string. which is a string that keyboard is holding.
		 * @param view The view component that is bounded with the keyboard. e.g. a EditText or Button. 
		 */
		public void onKeyboardExit(String stringHolding, View view);
	}
	

}
