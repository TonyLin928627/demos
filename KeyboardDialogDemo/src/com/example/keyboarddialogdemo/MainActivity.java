package com.example.keyboarddialogdemo;

import com.eight.keyboard.KeyboardDialog;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity implements OnClickListener {

    public static Typeface ligTf;
	public static Typeface regularTf;
	private Button btn;
	private TextView tv;
	private EditText et;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //load typeface
        MainActivity.ligTf = Typeface.createFromAsset(this.getAssets(),"fonts/Lato-Lig.ttf");
        MainActivity.regularTf = Typeface.createFromAsset(this.getAssets(),"fonts/Lato-Regular.ttf");

        this.tv = (TextView)this.findViewById(R.id.textView1);
        this.btn = (Button)this.findViewById(R.id.button1);
        this.et = (EditText)this.findViewById(R.id.editText1);
        
        //set type face
        this.tv.setTypeface(MainActivity.ligTf);
        this.btn.setTypeface(MainActivity.ligTf);
        this.et.setTypeface(MainActivity.ligTf);
        
        //set event listener
        this.tv.setOnClickListener(this);
        this.btn.setOnClickListener(this);
        this.et.setOnClickListener(this);
        
        
    }

	@Override
	public void onClick(View objectView) {
		//invoke the keyboard
		KeyboardDialog.activateKeyboard(this, objectView, new MyOnKeyboardExitListener(), new MyOnKeyboardExitListener1(), 0);
	}
	
	private class MyOnKeyboardExitListener implements KeyboardDialog.OnKeyboardExitListener{

		@Override
		public void onKeyboardExit(String stringHolding, View view) {
			
			//Show a message on screen to indicate the call back has executed
			Toast.makeText(MainActivity.this, "The content is "+ stringHolding + " now.", Toast.LENGTH_SHORT).show();
			
		}
		
	}
	private class MyOnKeyboardExitListener1 implements KeyboardDialog.OnContentChangedListener{

		@Override
		public void onContentChanged(String stringHolding, View view) {
			
			//Show a message on screen to indicate the call back has executed
			Toast.makeText(MainActivity.this, "Keyboard closed", Toast.LENGTH_LONG).show();

		}
		
	}

}
