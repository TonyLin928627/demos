package com.TonyLin.viewDemo;


import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import android.util.Log;

public class MainActivity extends Activity implements OnClickListener, OnCheckedChangeListener, OnSeekBarChangeListener {
	
	private CheckBox isFreeFallingModeCb;
	private EditText degreeEt;
	private FaceView faceView;
	private SeekBar scaleBar;
	private boolean isScaleEnable = true;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//set layout.
		DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        //Log.v("FaceView", "width="+dm.widthPixels+", height="+dm.heightPixels);
        int layoutId = dm.widthPixels>480?R.layout.activity_main:R.layout.activity_main_800_480;
       
		//set listeners of widgets.
		this.setContentView(layoutId);
	
		this.findViewById(R.id.exitBtn).setOnClickListener(this);
		
		
		this.faceView = (FaceView)this.findViewById(R.id.faceView);
		
		this.scaleBar = (SeekBar)this.findViewById(R.id.ScaleSb);
		this.scaleBar.setOnSeekBarChangeListener(this);
		this.scaleBar.setMax(FaceView.SCALE_RANGE*2+1);

		this.degreeEt = (EditText)this.findViewById(R.id.degreeEt);
		
		this.isFreeFallingModeCb = (CheckBox)this.findViewById(R.id.isFreeFallingModeCb);
		this.isFreeFallingModeCb.setOnCheckedChangeListener(this);
		this.isFreeFallingModeCb.requestFocus();
		
		this.faceView.setFreeFallingMode(this.isFreeFallingModeCb.isChecked());
	
		
		
		
		this.findViewById(R.id.rotateBtn).setOnClickListener(this);
		this.findViewById(R.id.resetBtn).setOnClickListener(this);
		this.findViewById(R.id.changePhotoBtn).setOnClickListener(this);
		this.findViewById(R.id.exitBtn).setOnClickListener(this);
		
	}


	@Override
	public void onClick(View view) {

		switch (view.getId()){
		
		case R.id.exitBtn: // when exit button is clicked.
		new AlertDialog.Builder(MainActivity.this)
				.setMessage("Sure to exit?")
				.setPositiveButton("No", null)
				.setNegativeButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								FaceView.chosenBm = null;
								MainActivity.this.finish();

							}
						}).show();
			break;	
		
		case R.id.rotateBtn:// when rotate button is clicked.
			try{		
				this.faceView.doRotate( Float.parseFloat(this.degreeEt.getText().toString()));
			}catch(NumberFormatException e){
				Toast.makeText(this.getApplicationContext(), "Please enter a number.", Toast.LENGTH_SHORT).show();
			}
			break;
			
		case R.id.changePhotoBtn: //// when photo button is clicked.
			
			try {  
				// Launch picker to choose photo for selected contact
				final Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);  
					intent.setType("image/*");  
					intent.putExtra("crop", "false");  
				//	intent.putExtra("aspectX", 1);  
				//	intent.putExtra("aspectY", 1);  
				//	intent.putExtra("outputX", 80);  
				//	intent.putExtra("outputY", 80);  
					intent.putExtra("return-data", true);  

					startActivityForResult(intent, 0);
			} catch (ActivityNotFoundException e) {
				Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
			}

			break;
		
		case R.id.resetBtn: //// when reset button is clicked.
			this.scaleBar.setProgress(FaceView.SCALE_RANGE+1);
			this.isFreeFallingModeCb.setChecked(false);
			this.faceView.doReset();
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
//		Log.v("FaceView", "requestCode="+requestCode);
//		Log.v("FaceView", "resultCode="+resultCode);
//		Log.v("FaceView", "getDataString="+data);
		
		this.isScaleEnable = true;
//		Log.v("FaceView", "isScaleEnable="+isScaleEnable+"---1");
		if (data==null) return;	//if user has not a photo.
		
		Bitmap sourceBitmap = (Bitmap)data.getExtras().get("data");
		Rect rect =  (Rect)data.getExtras().get("cropped-rect");
		
//		Log.v("FaceView", sourceBitmap+","+rect);
		this.faceView.changeBitmap(sourceBitmap, rect);
		
//		Object[] keySetArray = data.getExtras().keySet().toArray();
//		for(Object object : keySetArray)
//			Log.v("FaceView", object.toString()+"="+data.getExtras().get(object.toString()));
	}


	@Override
	// called when user try to change freeFallingMode by check/unchect the checkbox.
	public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
		if (isChecked){									//start free falling
			this.faceView.setFreeFallingMode(true);
			this.faceView.startFreeFalling();
		}else{											//stop free falling
			this.faceView.stopFreeFalling();
			this.faceView.setFreeFallingMode(false);
		}
		
		
	}


	@Override
	// called when user try to scale faceBm by pulling seekBar.
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
			
//		Log.v("FaceView", "this.isScaleEnable="+this.isScaleEnable);
		
		
		if(this.isScaleEnable)
			this.faceView.doScale(progress);
	}


	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}




	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		this.isScaleEnable = true;
		
//		Log.v("FaceView", "isScaleEnable="+isScaleEnable+"---2");
	}


	/* (non-Javadoc)
	 * @see android.app.Activity#onRestoreInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
	//	Log.v("FaceView", "onRestoreInstanceState()");
		
		this.isScaleEnable = true;
		
		Log.v("FaceView", "isScaleEnable="+isScaleEnable+"---3");
	}


	/* (non-Javadoc)
	 * @see android.app.Activity#onSaveInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
	//	Log.v("FaceView", "onSaveInstanceState()");
		this.isScaleEnable = false;
		
//		Log.v("FaceView", "isScaleEnable="+isScaleEnable+"---4");
	}



}
