package com.TonyLin.viewDemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

//import android.util.Log;

@SuppressLint("HandlerLeak")
public class FaceView extends View implements OnTouchListener{

//	action mode constants
	private static final byte DO_MOVE = 0;
	private static final byte DO_SCALE = 1;
	private static final byte DO_ROTATE = 2;
	private static final byte DO_CHANGE_BITMAP = 3;
	private static final byte DO_RESET = 4;
	
//	acceleration of gravity   
	private static final float G = 9.82f;

//	black ground color
//	private static final int BG_COLOR =Color.rgb(0,0,255); // blue
	private static final int BG_COLOR = Color.WHITE;

	static Bitmap chosenBm;
//	the reference to face Bitmap object. this object presents what finally the user can see on screen.	
	private Bitmap faceBm;
	
/*	the reference to face Bitmap object. this object presents what the face Bitmap original is. 
	everytime when faceBm is rotated or scaled, it bases on this object. */
	private Bitmap originalfaceBm;

// 	references of offscreen Bitmap & Canvas objects.	
	private Bitmap offScreenBm;
	private Canvas offScreenC;
	
//  constants and global variables needed for scaling, 
	static final int SCALE_RANGE = 9;	//means maximum enlarge or shrink 9 times from origin.	
	private static float SCALE = 1.1f;  //everytime enlarge or shrink 1.1 time from previous.
	private int currentScale = 0;		//presents how many times the current faceBm have been scaled from origin.		
	
//	global variables needed for rotation
	private float degreeSum;			//presents how many degrees totally the current faceBm have been scaled 
										//from origin. equals degreeSum%360 when it's over 360;
	
	
//===========================================
//  looper and handler
	private Looper mainLooper;
	private FaceViewHandler faceViewHandler;
//===========================================	

//  current coordinates of face bitmap.	
	private float x;
	private float y;

//	offsets from finger-touch-point to left-top of faceBm; 
	private float offsetX;
	private float offsetY;

// 	presents if the inger-touch-point is inside faceBm or not;
	boolean isOutOfFace = true;
	
//	when freeFallingMode is on, faceBm does free falling.	
	private boolean freeFallingMode = false;
	
//  presents if faceBm is free falling	
	private boolean isFreeFalling = false;

//	presents if faceBm is on bottom.	
	private boolean isBottomTouched = false;
	
//	store System.currentTimeMillis() when free falling action is about to start. need it to work out displacement.
	private long startFallingTime = 0;
	
// 	A paint object for most of drawXXX method.	
	private final Paint aPaint = new Paint();
	
	
	/* (non-Javadoc)
	 * @see android.view.View#onSizeChanged(int, int, int, int)
	 */
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
	//	Log.v("FaceView", "onSizeChanged() called");		
		
		// initialization of offScreenBm and offScreenC.
		this.offScreenBm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		
		this.offScreenC = new Canvas(this.offScreenBm);
		
		this.offScreenC.drawBitmap(this.faceBm, x, y, this.aPaint);
		
		
//		this.x = y = 0;
//		FaceView.this.currentScale = 0;
//		FaceView.this.degreeSum = 0;
//		FaceView.this.currentScale = 0;
//		FaceView.this.isBottomTouched = false;
//		FaceView.this.isFreeFalling = false;
//		FaceView.this.freeFallingMode = false;
		
	}
	


	
	public FaceView(Context context, AttributeSet attr){
		super(context, attr);
	//	Log.v("FaceView", "Construtor called");
		
		// initialization of originalfaceBm and faceBm.
		if (FaceView.chosenBm == null)
		this.originalfaceBm = BitmapFactory.decodeResource(this.getResources(), R.drawable.ym);
		else
			this.originalfaceBm = FaceView.chosenBm;
			
		this.faceBm =originalfaceBm;
		
		//setOnTouchListener for FaceView object.
		this.setOnTouchListener(this);
		
		//get looper of the main thread.
		this.mainLooper = Looper.getMainLooper();
		
		//create message handler for the looper
		this.faceViewHandler = new FaceViewHandler(this.mainLooper);
	}
		

	/* (non-Javadoc)
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		canvas.drawColor(FaceView.BG_COLOR);
			
		//do free falling 	
		if ((this.isFreeFalling) && (this.freeFallingMode)) {
			long currentTime = System.currentTimeMillis();
			float delta = currentTime - this.startFallingTime;
			
			//change unit from millisecond to second.
			float deltaSec = delta / 1000;
			
			//calculate the displacement
			y += (G * (deltaSec * deltaSec)) / 2;

			//exam if faceBm is going to touch or be under bottom.
			if (this.y >= (this.offScreenBm.getHeight() - this.faceBm.getHeight())) {
				
				this.y = (this.offScreenBm.getHeight() - this.faceBm.getHeight()); // pull faceBm on bottom when it is under bottom.

				this.isBottomTouched = true;	//set isBottomTouched=true means faceBm is currently on bottom.

				this.isFreeFalling = false;		//set isFreeFalling=true means faceBm is not doing free falling.

//				this.offScreenC.drawColor(FaceView.BG_COLOR);
//				this.offScreenC.drawText("X=" + x + ", Y=" + y + ", offsetX="+ offsetX + ", offsetY=" + offsetY, 10, 10, this.aPaint);
//				this.offScreenC.drawBitmap(FaceView.this.faceBm, this.x, y,this.aPaint);
			} 

			
			//==============draw everything on off-screen canvas====================
			this.drawOnOffscreenC();
//			//draw black ground
//			this.offScreenC.drawColor(FaceView.BG_COLOR);
//			//draw coordinates on top
//			this.offScreenC.drawText("X=" + x + ", Y=" + y + ", offsetX="+ offsetX + ", offsetY=" + offsetY, 10, 10, this.aPaint);
//			//draw faceBm
//			this.offScreenC.drawBitmap(FaceView.this.faceBm, x, y,this.aPaint);

			
			this.invalidate();

		}

			// we all know what this is for.
			canvas.drawBitmap(this.offScreenBm, 0,0, this.aPaint);
			
	}


	/* (non-Javadoc)
	 * @see android.view.View#onDragEvent(android.view.DragEvent)
	 */	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		
		switch(event.getAction()){
		//when finger down, set the offsets from left-top of face to touched point.
		case MotionEvent.ACTION_DOWN:
			offsetX = event.getX() - x;
			offsetY = event.getY() - y;
			
			//touched outside the face.
			if (offsetX>this.faceBm.getWidth() || offsetX<0 || offsetY>this.faceBm.getHeight() || offsetY<0) {
				this.isOutOfFace = true;	
			}else{
				this.isOutOfFace = false;
			}
			
			break;
			
		case MotionEvent.ACTION_UP:
			
			
			//when finger up, decide if the faceBm should do free falling 
			if((!this.isOutOfFace)&&(this.freeFallingMode)){
				this.startFreeFalling();
			}
			
			break;
			
		case MotionEvent.ACTION_MOVE:
			
			//only move faceBm when it is touched.
			if (!this.isOutOfFace){
				//use offset values to emulate a real drag.
				x = event.getX()-offsetX;
				y = event.getY()-offsetY;
				
			//============now start to calculate the coordinates of faceBm.==============
				//not letting faceBm move out of screen.
				if(x<0) x=0;
				if(y<0) y=0;
				if(x>this.offScreenBm.getWidth()-this.faceBm.getWidth()) x=this.offScreenBm.getWidth()-this.faceBm.getWidth();
				
				//when move to bottom, need to set isBottomTouched as well.
				if(y>this.offScreenBm.getHeight()-this.faceBm.getHeight()) {
					y=this.offScreenBm.getHeight()-this.faceBm.getHeight();
					this.isBottomTouched = true;
				}else{
					this.isBottomTouched = false;
				}
			//============================================================================	
				
			//send a message with coordinates to handler..
				Message msg = this.faceViewHandler.obtainMessage(FaceView.DO_MOVE);
				Bundle dataBundle = new Bundle();
				dataBundle.putFloat("X", x);
				dataBundle.putFloat("Y", y);
				msg.setData(dataBundle);
	
				this.faceViewHandler.sendMessage(msg);
			}
			
			break;

		}
		
		return true;
	}




	// message handler class definition.
	private class FaceViewHandler extends Handler {
		
		public FaceViewHandler(Looper mainLooper) {
			// TODO Auto-generated constructor stub
		}

		/* (non-Javadoc)
		 * @see android.os.Handler#handleMessage(android.os.Message)
		 * 
		 * this method is used to handle messages from the message queue.
		 */
		@Override
		public void handleMessage(Message msg) {
			
			super.handleMessage(msg);
		
			//matrix object, used for sacaling and rotation.
			Matrix matrix;
			//presents how many times should originalfaceBm be enlarged or shrinked to get faceBm.(negative means shrink) 
			float scale;
					
			switch (msg.what) {
				case FaceView.DO_MOVE:
					
					FaceView.this.x = msg.getData().getFloat("X");
					FaceView.this.y = msg.getData().getFloat("Y");
					
					//=====================================================
					// bg
					FaceView.this.offScreenC.drawColor(FaceView.BG_COLOR);
					
					// coordinates
					FaceView.this.offScreenC.drawText("X="+x+", Y="+y+", offsetX="+offsetX+", offsetY="+offsetY, 10, 10, FaceView.this.aPaint);
					
					//faceBm
					FaceView.this.offScreenC.drawBitmap(FaceView.this.faceBm, FaceView.this.x, FaceView.this.y, FaceView.this.aPaint);
					
					// invalidate
					FaceView.this.invalidate();
					
					break;
					
				case FaceView.DO_SCALE:		
					//update scale status
					FaceView.this.currentScale=msg.getData().getInt("SCALE")-(FaceView.SCALE_RANGE+1);
					
					//reset scale status when excess limit
					if (FaceView.this.currentScale>FaceView.SCALE_RANGE) FaceView.this.currentScale = FaceView.SCALE_RANGE;
					else if ((FaceView.this.currentScale<-FaceView.SCALE_RANGE))  FaceView.this.currentScale = -FaceView.SCALE_RANGE;
					
					//post to matrix object
					matrix = new Matrix(); 
					scale = (float) Math.pow(FaceView.SCALE, FaceView.this.currentScale);
					matrix.postScale(scale, scale);
					matrix.postRotate(FaceView.this.degreeSum);

					//===========================================================================
					//work out the updated face bitmap 
					FaceView.this.faceBm = Bitmap.createBitmap(FaceView.this.originalfaceBm,0,0,FaceView.this.originalfaceBm.getWidth(),FaceView.this.originalfaceBm.getHeight(),matrix,true);		
					
						// adjust coordinates when it is on floor.
					if(FaceView.this.isBottomTouched){
						if (FaceView.this.y+FaceView.this.faceBm.getHeight() !=  FaceView.this.offScreenBm.getHeight())
								FaceView.this.y = FaceView.this.offScreenBm.getHeight() - FaceView.this.faceBm.getHeight();	
					}
					
					
					FaceView.this.drawOnOffscreenC();
//					//draw background color on offscreen canvas
//					FaceView.this.offScreenC.drawColor(FaceView.BG_COLOR);
//					
//					// draw coordinates on the top 
//					FaceView.this.offScreenC.drawText("X="+x+", Y="+y+", offsetX="+offsetX+", offsetY="+offsetY, 10, 10, FaceView.this.aPaint);		
//					
//				
//					//draw face bitmap onto offscreen canvas
//					FaceView.this.offScreenC.drawBitmap(FaceView.this.faceBm, FaceView.this.x, FaceView.this.y, FaceView.this.aPaint);
//					
					
					FaceView.this.invalidate();
					
					break;
					
				case FaceView.DO_ROTATE:
					//update rotation status
					FaceView.this.degreeSum=(FaceView.this.degreeSum+msg.getData().getFloat("DEGREE"))%360;
					
					//post to matrix object
					matrix = new Matrix(); 
					scale = (float) Math.pow(FaceView.SCALE, FaceView.this.currentScale);
					matrix.postScale(scale, scale);
					matrix.postRotate(FaceView.this.degreeSum); 
			
					//===========================================================================
					//work out the updated face bitmap 
					FaceView.this.faceBm = Bitmap.createBitmap(FaceView.this.originalfaceBm, 0, 0,FaceView.this.originalfaceBm.getWidth(),FaceView.this.originalfaceBm.getHeight(), matrix, true);				
					
					// adjust coordinates when it is on floor.
					if(FaceView.this.isBottomTouched){
						if (FaceView.this.y+FaceView.this.faceBm.getHeight() !=  FaceView.this.offScreenBm.getHeight())
								FaceView.this.y = FaceView.this.offScreenBm.getHeight() - FaceView.this.faceBm.getHeight();	
					}
	
					FaceView.this.drawOnOffscreenC();
//					//draw background color on offscreen canvas
//					FaceView.this.offScreenC.drawColor(FaceView.BG_COLOR);
//					
//					// draw coordinates on the top 
//					FaceView.this.offScreenC.drawText("X=" + x + ", Y=" + y+ ", offsetX=" + offsetX + ", offsetY=" + offsetY, 10, 10, FaceView.this.aPaint);
//					
//					//draw face bitmap onto offscreen canvas
//					FaceView.this.offScreenC.drawBitmap(FaceView.this.faceBm,FaceView.this.x, FaceView.this.y, FaceView.this.aPaint);

					//draw rectangle that encloses face bitmap.
					Paint paint = new Paint();
					paint.setStyle(Style.STROKE);
					paint.setColor(Color.GREEN);
					FaceView.this.offScreenC.drawRect(FaceView.this.x,FaceView.this.y,FaceView.this.faceBm.getWidth()+ (int)FaceView.this.x, FaceView.this.faceBm.getHeight()+(int)FaceView.this.y,paint);
					FaceView.this.offScreenC.drawText(FaceView.this.degreeSum+"��", FaceView.this.faceBm.getWidth()+ (int)FaceView.this.x, FaceView.this.y, paint);
					
					
					FaceView.this.invalidate();

					break;
						
					
				case FaceView.DO_CHANGE_BITMAP:
					Bitmap chosenBm = (Bitmap)msg.obj;
					
					//abort when the bitmap is bigger than scrren.
					if((chosenBm.getWidth()>=FaceView.this.offScreenBm.getWidth())||(chosenBm.getHeight()>=FaceView.this.offScreenBm.getHeight()))
						break;
					
					//set value of FaceView.chosenBm
					FaceView.chosenBm = chosenBm;
					
					//reset status for a new bitmap.
					FaceView.this.currentScale = 0;
					FaceView.this.degreeSum = 0;
					
					//reassign values for originalfaceBm and  faceBm
					FaceView.this.faceBm = FaceView.this.originalfaceBm = (Bitmap)msg.obj;	
					
					//adjust coordinates
					if(FaceView.this.isBottomTouched){
						if (FaceView.this.y+FaceView.this.faceBm.getHeight() !=  FaceView.this.offScreenBm.getHeight())
								FaceView.this.y = FaceView.this.offScreenBm.getHeight() - FaceView.this.faceBm.getHeight();	
					}
					
					FaceView.this.drawOnOffscreenC();
//					//work out what offscreen bitmap should look like. same as other cases. 
//					FaceView.this.offScreenC.drawColor(FaceView.BG_COLOR);
//					
//					//draw coordinates info
//					FaceView.this.offScreenC.drawText("X="+x+", Y="+y+", offsetX="+offsetX+", offsetY="+offsetY, 10, 10, FaceView.this.aPaint);
//						
//
//					FaceView.this.offScreenC.drawBitmap(FaceView.this.faceBm, FaceView.this.x, FaceView.this.y, FaceView.this.aPaint);
						
					FaceView.this.invalidate();
					
					break;
					
				case FaceView.DO_RESET:
					//reset 
					FaceView.this.x = FaceView.this.y = 0;
					FaceView.this.currentScale = 0;
					FaceView.this.degreeSum = 0;
					FaceView.this.currentScale = 0;
					FaceView.this.isBottomTouched = false;
					FaceView.this.isFreeFalling = false;
					FaceView.this.freeFallingMode = false;
					
					FaceView.this.faceBm = FaceView.this.originalfaceBm;
					
					FaceView.this.drawOnOffscreenC();
					
//					FaceView.this.offScreenC.drawColor(FaceView.BG_COLOR);
//					FaceView.this.offScreenC.drawText("X="+x+", Y="+y+", offsetX="+offsetX+", offsetY="+offsetY, 10, 10, FaceView.this.aPaint);
//					FaceView.this.offScreenC.drawBitmap(FaceView.this.faceBm, FaceView.this.x, FaceView.this.y, FaceView.this.aPaint);
//					
					FaceView.this.invalidate();
			}
		}
		
	}
	
	//start free falling
	 void startFreeFalling(){
		this.isFreeFalling = true;
		this.startFallingTime = System.currentTimeMillis();
		this.invalidate();
		
	}

	 //stop free falling
	void stopFreeFalling(){
		this.isFreeFalling = false;
	}

	
	/**
	 * @return the freeFallingMode
	 */
	boolean isFreeFallingMode() {
		return freeFallingMode;
	}


	/**
	 * @param freeFallingMode the freeFallingMode to set
	 */
	void setFreeFallingMode(boolean freeFallingMode) {
		this.freeFallingMode = freeFallingMode;
	}


	// send msg to handler asking to do scale
	void doScale(int scale){
		Message msg = this.faceViewHandler.obtainMessage(FaceView.DO_SCALE);
		Bundle dataBundle = new Bundle();
		dataBundle.putInt("SCALE", scale);
		msg.setData(dataBundle);
		this.faceViewHandler.sendMessage(msg);
	}



	//send msg to handler asking to do rotate
	void doRotate(float degree) {
		Message msg = this.faceViewHandler.obtainMessage(FaceView.DO_ROTATE);
		Bundle dataBundle = new Bundle();
		dataBundle.putFloat("DEGREE", degree);
		msg.setData(dataBundle);
		this.faceViewHandler.sendMessage(msg);
	}

	
	//send msg to handler asking tochange face bitmap
	void changeBitmap(Bitmap sourceBm,Rect rect) {

		Message msg = this.faceViewHandler.obtainMessage(FaceView.DO_CHANGE_BITMAP);
		
		msg.obj = sourceBm;
		this.faceViewHandler.sendMessage(msg);
	}



	//send msg to handler asking to reset face bitmap
	public void doReset() {
		Message msg = this.faceViewHandler.obtainMessage(FaceView.DO_RESET);

		this.faceViewHandler.sendMessage(msg);
		
	}

	//==============draw everyting on offscreen canvas====================
	private void drawOnOffscreenC(){
		//draw black ground
		this.offScreenC.drawColor(FaceView.BG_COLOR);
		//draw coordinates on top
		this.offScreenC.drawText("X=" + x + ", Y=" + y + ", offsetX="+ offsetX + ", offsetY=" + offsetY, 10, 10, this.aPaint);
		//draw faceBm
		this.offScreenC.drawBitmap(FaceView.this.faceBm, x, y,this.aPaint);
	}
}

