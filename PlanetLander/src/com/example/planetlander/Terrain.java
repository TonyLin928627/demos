package com.example.planetlander;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Path;


public class Terrain {	
	private final int SAVE_LAND_ANGLE = 5;
	
	private float highest = 0.4f;
	private float lowest = 0.1f;
	private float maxSpan_y = 50f;
	private float maxSpan_x = 200f;
	private int canvasW;
	private int canvasH;
	private int spacecraftWidth;
	
	private float[] xArray;
	private float[] yArray;
	
	private List<LandPoint> landPointsList;
	
	private Path path;
	private static Terrain instance;
	

	public float getHighest() {
		return highest;
	}


	/**
	 * @param highest
	 * @param lowest
	 * @param maxSpan_y
	 * @param maxSpan_x
	 * @param canvasW
	 * @param canvasH
	 */
	private Terrain(float highest, float lowest, float maxSpan_x,
			float maxSpan_y, int canvasW, int canvasH,int spacecraftWidth) {
		super();
		this.highest = highest;
		this.lowest = lowest;
		this.maxSpan_y = maxSpan_y;
		this.maxSpan_x = maxSpan_x;
		this.canvasW = canvasW;
		this.canvasH = canvasH;
		this.spacecraftWidth = spacecraftWidth;
	}


	private Terrain(){
		super();
	}
	
	
	public static Terrain createTerrain(float highest, float lowest, float maxSpan_x, float maxSpan_y, int canvasW, int canvasH, int spacecraftWidth) {
		Terrain.instance = new Terrain(highest,lowest,maxSpan_x,maxSpan_y,canvasW,canvasH, spacecraftWidth);
		return Terrain.instance;
		
	}
	
	public static Terrain buildTerrain(float highest, float lowest, float maxSpan_x, float maxSpan_y, int canvasW, int canvasH, int spacecraftWidth) {
		if (Terrain.instance==null)
			Terrain.instance = createTerrain(highest,lowest,maxSpan_x,maxSpan_y,canvasW,canvasH, spacecraftWidth);
		
		return Terrain.instance;
	}
	
	public static Terrain buildTerrain(){
		return Terrain.instance;
	}

	private Path makePath(){
		
		List xList = new ArrayList();
		List yList = new ArrayList();
		this.path = new Path();
		float highestY = canvasH*(1-this.highest);
		float lowestY = canvasH*(1-this.lowest);
				
		float x = 0;
		float y = canvasH;
		path.moveTo(x, y);
		xList.add(x);
		yList.add(y);
		
		int random_i = (int)((Math.random()*1000)%13)+2;
		
	//	boolean isLandPointSet = false;
		for(int loopCount=0; true; loopCount++){	
			float deltaY = 0;
			
			//creat y
			if (loopCount!=random_i){
				deltaY = (((float)Math.random()*maxSpan_y*10)%maxSpan_y);
			
				if(((int)deltaY)%2==1){
					y+=deltaY;
				}else{
					y-=deltaY;
				}
			}else{
			//	Log.v("Terrain",loopCount+":random_i="+random_i );
			}
			
			while((y<highestY)||(y>lowestY)){
				deltaY = (((float)Math.random()*maxSpan_y*10)%maxSpan_y);
				if (y<highestY) y+=deltaY;
				else if(y>lowestY) y-=deltaY;	
			}
			
			
			//add path 
			if(x>=canvasW){
				x = canvasW;
				path.lineTo(x, y);
				xList.add(x);
				yList.add(y);
				break;
			}else{
				path.lineTo(x, y);
				xList.add(x);
				yList.add(y);
			}
			
		//	Log.v("Terrain","("+x+","+y+")" );
			
			//creat x for next loop
			float deltaX = ((float)(Math.random()*maxSpan_x*10))%maxSpan_x;
			x+= deltaX;
				
		}
		
		path.lineTo(canvasW, canvasH);
		xList.add(x);
		yList.add(y);
		
		path.lineTo(0, canvasH);
		xList.add(x);
		yList.add(y);
		
        path.close();
        
        this.xArray = new float[xList.size()];
        this.yArray = new float[yList.size()];
        
        this.landPointsList = new ArrayList<LandPoint>();
		for(int i=0; i<xArray.length; i++){
			xArray[i] = ((Float)xList.get(i)).floatValue();
			yArray[i] = ((Float)yList.get(i)).floatValue();
			
			
			if(i==0) continue;
			float spanX = xArray[i]-xArray[i-1];
			if(spanX>=this.spacecraftWidth){
				float spanY = yArray[i]-yArray[i-1];
				double angle = (Math.atan((spanY) / (spanX))) ;
				angle=Math.abs(angle*(180/3.14));
				if(angle<SAVE_LAND_ANGLE){
					LandPoint lp = new LandPoint(xArray[i-1],xArray[i],yArray[i-1],yArray[i-1]);
					this.landPointsList.add(lp);
				}
			}
			
		}
		
//		this.landPointsArray = new int[landPaintsList.size()];
//		for (int i=0; i<landPaintsList.size(); i++){
//			this.landPointsArray[i] = ((Integer)landPaintsList.size()).intValue();
//		}
     return this.path;   
	}
	
	public Path getPath(){
		if (this.path!=null) return this.path;
		
		do{
			this.path = this.makePath();
		}
		while(this.landPointsList.size()==0);
			
		
		
		return this.path;
	}


	public static float[] doTouchDetection(float spacecraftX, float spacecraftY, int bmWidth, int bmHeight) {
		float top_left_x = spacecraftX;
		float top_left_y = spacecraftY;
		
		float bottom_left_x = spacecraftX;
		float bottom_left_y = spacecraftY+bmHeight;
				
		float top_right_x = spacecraftX+bmWidth;
		float top_right_y = spacecraftY;
		
		float bottom_right_x = spacecraftX+bmWidth;
		float bottom_right_y = spacecraftY+bmHeight;
		
		if (Terrain.contain(bottom_left_x, bottom_left_y, Terrain.instance.xArray, Terrain.instance.yArray)){
			float[] rt = {bottom_left_x, bottom_left_y};
			return rt;
		}
		
		if (Terrain.contain(bottom_right_x, bottom_right_y, Terrain.instance.xArray, Terrain.instance.yArray)){
			float[] rt = {bottom_right_x, bottom_right_y};
			return rt;
		}
		
		if (Terrain.contain(top_left_x, top_left_y, Terrain.instance.xArray, Terrain.instance.yArray)){
			float[] rt = {top_left_x, top_left_y};
			return rt;
		}
		
		if (Terrain.contain(top_right_x, top_right_y, Terrain.instance.xArray, Terrain.instance.yArray)){
			float[] rt = {top_right_x, top_right_y};
			return rt;
		}
			
		return null;
		
	}
	
	private static boolean contain(float x, float y, float[] xArray, float[] yArray){
		int crossCount = 0;
		
		for(int i=0; i<xArray.length-2; i++){
			float greaterY = Math.max(yArray[i+1],yArray[i]);
			float lessY = Math.min(yArray[i+1],yArray[i]);
			
			if (x>xArray[i]&&(y>lessY&&y<greaterY)) crossCount++;
		}
		
		
		return (crossCount%2==1)?true:false;
		
	}
	
	
	
	public float[] getxArray() {
		return xArray;
	}


	public List<LandPoint> getLandPointsList() {
		return landPointsList;
	}



	class LandPoint{
		private float startX;
		private float stopX;
		private float startY;
		private float stopY;
		
		private boolean isPrevLandPoint;
		private boolean isNextLandPoint;
		public float getStartX() {
			return startX;
		}
		public float getStopX() {
			return stopX;
		}
		
		
		public float getStartY() {
			return this.startY;
		}
		
		public float getStopY() {
			return this.stopY;
		}
		
		public boolean isPrevLandPoint() {
			return isPrevLandPoint;
		}
		public boolean isNextLandPoint() {
			return isNextLandPoint;
		}
	
		/**
		 * @param startX
		 * @param stopX
		 * @param isPrevLandPoint
		 * @param isNextLandPoint
		 */
		private LandPoint(float startX, float stopX,float y, boolean isPrevLandPoint,
				boolean isNextLandPoint) {
			super();
			this.startX = startX;
			this.stopX = stopX;
			this.startY = startY;
			this.stopY = stopY;
			this.isPrevLandPoint = isPrevLandPoint;
			this.isNextLandPoint = isNextLandPoint;
		}
		/**
		 * @param startX
		 * @param stopX
		 */
		private LandPoint(float startX, float stopX, float startY, float stopY) {
			super();
			this.startX = startX;
			this.stopX = stopX;
			this.startY = startY;
			this.stopY = stopY;
		}
	}
}
