package com.example.planetlander;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import android.util.Log;

public class SubmisionSfcView extends SurfaceView implements SurfaceHolder.Callback{
	private static final float HORI_MOVE_SPEED = 2.0f;
	private static final float VERT_MOVE_SPEED = 1.5f;
	
	private SurfaceHolder  holder;
	private MediaPlayer bgPlayer;
	
	private DrawThread drawThread;
	
	//spacescraft
	
	
	public SubmisionSfcView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		this.holder = this.getHolder();
		this.holder.addCallback(this);
		
		this.bgPlayer = MediaPlayer.create(this.getContext(), R.raw.bg);
		this.bgPlayer.setLooping(true);
		this.bgPlayer.setVolume(0.5f, 0.5f);
		
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {	
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		
		if (this.drawThread==null){
			this.drawThread = new DrawThread(holder, 120.0f, 80.0f);
			this.drawThread.start();
			
		//	this.bgPlayer.start();
		}
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		
		this.bgPlayer.stop();
		
		try {
			this.drawThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		this.drawThread = null;
		
		
	}
	
	public void showHelpLines(boolean b){
		this.drawThread.setShowHelpLines(b);
	}
	
	public void leftThrusterOn(boolean isOn){
		this.drawThread.leftThrusterOn = isOn;
		if(!isOn)
			this.drawThread.stopThrusterSound();
		
	}
	
	public void rightThrusterOn(boolean isOn){
		this.drawThread.rightThrusterOn = isOn;
		if(!isOn)
			this.drawThread.stopThrusterSound();
	}
	
	public void mainThrusterOn(boolean isOn){
		this.drawThread.mainThrusterOn = isOn;
		if(!isOn)
			this.drawThread.stopThrusterSound();
	}
	
	private class DrawThread extends Thread{
		
		private SurfaceHolder surfaceHolder;
		
		private static final float MARS_G =  3.72f/3;
		private long startFallingTime;
		
		private short startCount=0;
		private boolean isGameStarted = false;
		private boolean isGamePaused = false;
		private boolean isGameStoped = true;
		
		//for terrain drawing
		private float maxSpan_x;
		private float maxSpan_y;
		
		private Resources resources;
		
		//---------------------------------------
		private boolean leftThrusterOn = false;
		private boolean mainThrusterOn = false;
		private boolean rightThrusterOn = false;
		
		private float spacecraftX;
		private float spacecraftY;
		
		private float planetX = 40.0f;
		
		private Bitmap spacecraftBm;
		private Bitmap mainFlameBm;
		private Bitmap thrusterBm;
		private Bitmap landIndicatorBm;	
		private Bitmap explosionBm;
		private Bitmap gameOverBm;
		private Bitmap wonBm;
		private Bitmap oneBm;
		private Bitmap twoBm;
		private Bitmap threeBm;
		private Bitmap goBm;
		
		private float gameOverX;
		private float gameOverY;
		
		private float wonX;
		private float wonY;
		
		private final Paint aPaint = new Paint();
		private final Paint fuelTankPaint = new Paint();
		
		private final float HIGHEST_Y;
		
		private boolean isDrawIndicator = true;
		private boolean isShowHelpLines = false;
		
		private boolean isLanded = false;
		private boolean isCrashed = false;	
		
		private SoundPool soundPool; 
		private int sourceId_1;
		private int sourceId_2;
		private int sourceId_3;
		private int sourceId_4;
		private int sourceId_5;
		private int sourceId_6;
		
		private float[] crashPoint;

		private List drawableList = new ArrayList();

		private float[][] landIndicatorCoords;

		private final int FULL_TANK_LENGTH = 400;
		private final int FUEL_TANK_HEIGHT = 50;
		private float fuelLength = FULL_TANK_LENGTH;
		private int fuelTankX;
		private int fuelTankY;

		private boolean isWarnningSoundOn=false;
		private boolean isThrusterSoundOn = false;
		
		public void stopThrusterSound(){
			this.isThrusterSoundOn = false;
			this.soundPool.stop(this.sourceId_4);
		}
		
		/**
		 * @param surfaceHolder
		 * @param maxSpan_x
		 * @param maxSpan_y
		 * @param resources
		 */
		private DrawThread(SurfaceHolder surfaceHolder, float maxSpan_x,
				float maxSpan_y) {
			super();
			this.surfaceHolder = surfaceHolder;
			this.maxSpan_x = maxSpan_x;
			this.maxSpan_y = maxSpan_y;
			this.resources = SubmisionSfcView.this.getResources();
				 
			
		    this.soundPool = new SoundPool(5, AudioManager.STREAM_SYSTEM, 5);
		    this.sourceId_1 = soundPool.load(SubmisionSfcView.this.getContext(), R.raw.explosion, 0);
		    this.sourceId_2 = soundPool.load(SubmisionSfcView.this.getContext(), R.raw.landed, 0);
		    this.sourceId_3 = soundPool.load(SubmisionSfcView.this.getContext(), R.raw.warning, 0);
		    this.sourceId_4 = soundPool.load(SubmisionSfcView.this.getContext(), R.raw.thruster, 0);
		    this.sourceId_5 = soundPool.load(SubmisionSfcView.this.getContext(), R.raw.won, 0);
		    this.sourceId_6 = soundPool.load(SubmisionSfcView.this.getContext(), R.raw.go, 0);
			
			Canvas  aCanvas  =  this.surfaceHolder.lockCanvas();
			int canvasW = aCanvas.getWidth();
			int canvasH = aCanvas.getHeight();
			this.surfaceHolder.unlockCanvasAndPost(aCanvas); 
			
			this.spacecraftX = canvasW/2;
			
			this.fuelTankX = 165;
			this.fuelTankY = canvasH -this.FUEL_TANK_HEIGHT-18;
			
			//0.space
			Path bgPath = new Path();
			bgPath.moveTo(0, 0);
			bgPath.lineTo(0, canvasH);
			bgPath.lineTo(canvasW, canvasH);
			bgPath.lineTo(canvasW, 0);
			bgPath.close();
			ShapeDrawable spaceDrawable = new ShapeDrawable(new PathShape(bgPath, 1, 1));
			Bitmap spaceBm = BitmapFactory.decodeResource(this.resources, R.drawable.space);
			Shader spaceShader = new BitmapShader(spaceBm, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
			spaceDrawable.getPaint().setShader(spaceShader); 
			spaceDrawable.setBounds(0,0,1,1);
			this.drawableList.add(spaceDrawable);
			
			//1.planet
			Bitmap planetBm = BitmapFactory.decodeResource(this.resources, R.drawable.earth);
			this.drawableList.add(planetBm);
			
			//2. terrain
			this.spacecraftBm = BitmapFactory.decodeResource(this.resources, R.drawable.craftmain);
			Terrain terrain = Terrain.createTerrain(0.4f,0.2f,this.maxSpan_x,this.maxSpan_y,canvasW, canvasH,this.spacecraftBm.getWidth());
		//	Terrain terrain = Terrain.buildTerrain(0.4f,0.2f,this.maxSpan_x,this.maxSpan_y,canvasW, canvasH);
			Path terrainPath = terrain.getPath();
			ShapeDrawable terrainDrawable = new ShapeDrawable(new PathShape(terrainPath, 1, 1));
			Bitmap terrainBm = BitmapFactory.decodeResource(this.resources, R.drawable.mars);
			Shader terrainShader = new BitmapShader(terrainBm, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
			terrainDrawable.getPaint().setShader(terrainShader);     
			terrainDrawable.setBounds(0,0,1,1); 
			this.drawableList.add(terrainDrawable);
			this.HIGHEST_Y = canvasH*(1-terrain.getHighest());
			
			
			//3. control pad
		//	this.drawableList.add(this.cp.getShapeBm());
			
			//4. land indicator
			this.landIndicatorBm = BitmapFactory.decodeResource(this.resources, R.drawable.indicator);
			this.drawableList.add(this.landIndicatorBm);
			List<Terrain.LandPoint> landPointsList =  Terrain.buildTerrain().getLandPointsList();
			this.landIndicatorCoords = new float[landPointsList.size()][2];
			for(int i=0; i<landPointsList.size(); i++){
				Terrain.LandPoint lp = landPointsList.get(i);
				float[] landPointArray = {((lp.getStopX()-lp.getStartX())-landIndicatorBm.getWidth())/2+lp.getStartX(),
											Math.min(lp.getStartY(),lp.getStopY()),
											lp.getStartX(),
											lp.getStartY(),
											lp.getStopX(),
											lp.getStopY(),
											};
				this.landIndicatorCoords[i] = landPointArray;
				
			}
			
			//5.spacecraft
			//	this.spacecraftBm = BitmapFactory.decodeResource(this.resources, R.drawable.craftmain);
				this.mainFlameBm = BitmapFactory.decodeResource(this.resources, R.drawable.main_flame);
				this.thrusterBm = BitmapFactory.decodeResource(this.resources, R.drawable.thruster);
				this.drawableList.add(this.spacecraftBm);
				
						
				this.explosionBm = BitmapFactory.decodeResource(this.resources, R.drawable.crash);
				this.gameOverBm = BitmapFactory.decodeResource(this.resources, R.drawable.gameover);
				this.wonBm = BitmapFactory.decodeResource(this.resources, R.drawable.won);
				
				this.gameOverX = (canvasW-this.gameOverBm.getWidth())/2;
				this.gameOverY = (canvasH-this.gameOverBm.getHeight())/2-100;
				
				this.wonX = (canvasW-this.wonBm.getWidth())/2;
				this.wonY = (canvasH-this.wonBm.getHeight())/2-100;
				
				this.oneBm = BitmapFactory.decodeResource(this.resources, R.drawable.one);
				this.twoBm = BitmapFactory.decodeResource(this.resources, R.drawable.two);
				this.threeBm = BitmapFactory.decodeResource(this.resources, R.drawable.three);
				this.goBm = BitmapFactory.decodeResource(this.resources, R.drawable.zero);
				
		}


		private void setShowHelpLines(boolean isShowHelpLines) {
			this.isShowHelpLines = isShowHelpLines;
		}


		@Override
		public void run() {
			//this.startFallingTime = System.currentTimeMillis();
			boolean initStartFallingTimeDone =false;
			this.isGameStoped = false;
			while(!this.isGameStoped){
	        	
		        Canvas  canvas  =  this.surfaceHolder.lockCanvas(); 
		        
		      
		        if(canvas==null) return;
		        
		        long curTime = System.currentTimeMillis();
	        	this.doDrawing(canvas);  
	        	Log.v("drawTime", ""+(System.currentTimeMillis()-curTime));
	 
	        	this.surfaceHolder.unlockCanvasAndPost(canvas); 
	        	
	       
	        	if(this.startCount<=4){
	        		try {
	        		//	Log.v("sleep", "start");
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        		this.startCount++;
	        	//	Log.v("sleep", "end");
	        	}else{
	        		if(!initStartFallingTimeDone){
	        			this.startFallingTime = System.currentTimeMillis();
	        			initStartFallingTimeDone = true;
	        		}
	        		this.isGameStarted = true;
	        	}
	        }
	        
	   }


		private synchronized void doDrawing(Canvas canvas) {
			//0. draw space background.
			((ShapeDrawable)this.drawableList.get(0)).draw(canvas);
			
			//1. draw planet
			Bitmap planetBm = ((Bitmap)this.drawableList.get(1));
			canvas.drawBitmap(planetBm, this.planetX,40, null);
			if(!this.isCrashed&&this.isGameStarted){
				this.planetX+=0.2;
			
				if(this.planetX>canvas.getWidth()) this.planetX = (planetBm.getWidth()*-1);
			}
			
			
			//2. draw terrain   
			((ShapeDrawable)this.drawableList.get(2)).draw(canvas);
			
			if(this.isCrashed){// not drawing flowing when crashed
				this.aPaint.setColor(Color.BLACK);
				canvas.drawCircle(this.crashPoint[0]+(this.explosionBm.getWidth()/2), this.crashPoint[1]+(this.explosionBm.getHeight()/2), 55, this.aPaint);
				canvas.drawBitmap(this.explosionBm,this.crashPoint[0], this.crashPoint[1], null);
				canvas.drawBitmap(this.gameOverBm, this.gameOverX, this.gameOverY, null);
				this.isGameStoped = true;
				return;
			}
			
			if(this.isLanded){ // not drawing flowing when landed successfully
				canvas.drawBitmap(this.wonBm,this.wonX, this.wonY, null);
				canvas.drawBitmap(this.spacecraftBm, this.spacecraftX,this.spacecraftY,null);
				this.isGameStoped = true;
				return;
			}
	
		//	Log.v("isGameStarted","isGameStarted="+isGameStarted);
			if(this.isGameStarted){ // calculate the position of spacecraft when the game is started
				//go up button is pressed, raise the spacecraft
				if(this.mainThrusterOn&&this.fuelLength>0){
					//play sound of thruster
					if(!this.isThrusterSoundOn){
						this.soundPool.play(this.sourceId_4, 0.5f, 0.5f, 0, 0, 1);
						this.isThrusterSoundOn = true;
					}
					
					//work out y
					this.spacecraftY-=VERT_MOVE_SPEED;
					//reach the top
					if(this.spacecraftY<=0) this.spacecraftY = 0;

					//draw main thruster
					float mainFlameX = this.spacecraftX+ (this.spacecraftBm.getWidth() - this.mainFlameBm.getWidth()) / 2;
					float mainFlameY = this.spacecraftY + this.spacecraftBm.getHeight() + 2;
					canvas.drawBitmap(this.mainFlameBm, mainFlameX,mainFlameY, this.aPaint);
					
					this.isLanded = false;
					
					//consume fuel
					this.fuelLength-= 0.8f;
					
					this.startFallingTime = System.currentTimeMillis();
				}
				
				else if(!this.isLanded){  // go up button is not pressed, do free falling
					//work out the duration from when free falling started.
					float deltaTime = System.currentTimeMillis() - this.startFallingTime;
					
					//change unit from millisecond to second.
					float deltaSec = deltaTime / 1000;
					
					//calculate the displacement
					float deltaY = (MARS_G * (deltaSec * deltaSec)) / 2;
					this.spacecraftY += deltaY;
					
					
					int bottomY = canvas.getHeight() - this.spacecraftBm.getHeight();
					if (this.spacecraftY >= bottomY) {
						this.spacecraftY = bottomY;
					} 
				}
					
				
				//shift to right; 
				if (this.leftThrusterOn&&!this.isLanded&&this.fuelLength>0){
					//play thruster sound effect
					if(!this.isThrusterSoundOn){
						this.soundPool.play(this.sourceId_4, 0.2f, 0.2f, 0, 0, 1);
						this.isThrusterSoundOn = true;
					}
					
					//work out x
					this.spacecraftX+=HORI_MOVE_SPEED;
				
					//appear on right when disappear on left
					if (this.spacecraftX > canvas.getWidth())
						this.spacecraftX = 0;
					
					//draw left thruster
					float thrusterX = this.spacecraftX;
					float thrusterY = this.spacecraftY+this.spacecraftBm.getHeight();
					canvas.drawBitmap(this.thrusterBm, thrusterX,thrusterY, this.aPaint);
					
				
					//consume fuel
					this.fuelLength-= 0.6f;
				}
				
				
				//shift to left
				else if (this.rightThrusterOn&&!this.isLanded&&this.fuelLength>0){
					//play sound effect 
					if(!this.isThrusterSoundOn){
						this.soundPool.play(this.sourceId_4, 0.2f, 0.2f, 0, 0, 1);
						this.isThrusterSoundOn = true;
					}
					
					//work you x
					this.spacecraftX-=HORI_MOVE_SPEED;
					//appear on left when disappear on right
					if (this.spacecraftX<=0) this.spacecraftX = canvas.getWidth();
					
					//draw right thrust
					float thrusterX = this.spacecraftX+this.spacecraftBm.getWidth()-this.thrusterBm.getWidth();
					float thrusterY = this.spacecraftY+this.spacecraftBm.getHeight();
					canvas.drawBitmap(this.thrusterBm, thrusterX,thrusterY, this.aPaint);
					
					//consume fuel
					this.fuelLength-= 0.6f;
				}	

			}//end of calculate the position of spacecraft
			
			//4. draw land indicator
			boolean inSaveColumn = false;	//flag that shows if the spacecraft is currently in a "save Column", the column(s) can be seen when "HELP" button is pressed. 
			if(this.isDrawIndicator){
				//draw land point indicators. at lest one is available.
				for(float[] coord : this.landIndicatorCoords){	
					canvas.drawBitmap(this.landIndicatorBm, coord[0], coord[1], null);
					if(this.isShowHelpLines){
						this.aPaint.setColor(Color.WHITE);
						canvas.drawLine(coord[2], 0, coord[2], coord[3], this.aPaint);
						canvas.drawLine(coord[4], 0, coord[4], coord[5], this.aPaint);
					}
					
					//set value of inSaveColumn.
					if(this.spacecraftX>(coord[2])&&((this.spacecraftX+this.spacecraftBm.getWidth())<(coord[4]))){
						inSaveColumn = true;
					}
				}
			}// end of draw land indicators		
				
			
			//5.  draw spacecraft or crash site
				if(this.isGameStarted){
				float[] touchTerrainPoint = null; //flag that shows if the spacecraft has entered the terrain area.
			
				float bottomOfCraftY  = this.spacecraftY+this.spacecraftBm.getHeight();//y of the bottom of spacecraft
				if(bottomOfCraftY>this.HIGHEST_Y){ //terrain won't be higher than this.HIGHEST_Y,so only check it when the spacecraft is lower than that.
					touchTerrainPoint = Terrain.doTouchDetection(this.spacecraftX, this.spacecraftY, this.spacecraftBm.getWidth(),this.spacecraftBm.getHeight());
				}
			
				if(touchTerrainPoint==null){	//has not entered terrain area.
					if(inSaveColumn&&this.isShowHelpLines){	
					this.aPaint.setAlpha(100);
						canvas.drawBitmap(this.spacecraftBm, this.spacecraftX, this.spacecraftY, this.aPaint);
					
					}else{
						this.aPaint.setAlpha(255);
						canvas.drawBitmap(this.spacecraftBm, this.spacecraftX, this.spacecraftY, this.aPaint);
					}
				}else{	// crashed or landed successfully
					float landSpeed = 0;
					if(!this.isLanded){
						//work out the speed when land.
						landSpeed = MARS_G * (System.currentTimeMillis()-this.startFallingTime);
					}
				
					if(inSaveColumn&&landSpeed<3000){ //at save speed. so landed successfully
						canvas.drawBitmap(this.spacecraftBm, this.spacecraftX, this.spacecraftY, this.aPaint);
						if(! this.isLanded){
							this.soundPool.play(this.sourceId_2, 0.1f, 0.1f, 0, 0, 1);
							this.isLanded = true;
						
							try { //wait until the landing sound finish.
							Thread.sleep(800);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
							SubmisionSfcView.this.bgPlayer.stop();
						
							this.soundPool.autoPause();
						
							this.soundPool.play(this.sourceId_5, 0.3f, 0.3f, 3, 0, 1);
						}
					}else{ //speed is too high. so crashed
						this.isCrashed = true;
						this.crashPoint = new float[2];
					
						this.crashPoint[0] = this.spacecraftX-((this.explosionBm.getWidth()-this.spacecraftBm.getWidth())/2);
						this.crashPoint[1] = this.spacecraftY-((this.explosionBm.getHeight()-this.spacecraftBm.getHeight())/2)+25;
						
					
						SubmisionSfcView.this.bgPlayer.stop();
					
						this.soundPool.autoPause();
					
						this.soundPool.play(this.sourceId_1, 0.3f, 0.3f, 3, 0, 1);
						
					}	
				} 
			}//end of draw spacecraft or crash site
			
			// 6. draw fuelTank
			this.fuelTankPaint.setColor(Color.GRAY);
			canvas.drawRect(this.fuelTankX, this.fuelTankY, this.fuelTankX+this.FULL_TANK_LENGTH+20, this.fuelTankY+this.FUEL_TANK_HEIGHT, this.fuelTankPaint);
			
			//draw fuel level and warn when level is low.
			if(this.fuelLength>0){ //fuel has not run out.
				if(fuelLength>200){ 
					this.fuelTankPaint.setColor(Color.GREEN);
				}else if(fuelLength>100) {
					this.fuelTankPaint.setColor(Color.YELLOW);
				}else if(fuelLength>0) {
					this.fuelTankPaint.setColor(Color.RED);
					if(!this.isWarnningSoundOn){
						this.soundPool.play(this.sourceId_3, 0.5f, 0.5f, 0, -1, 1);
						this.isWarnningSoundOn = true;
					}
				}
					
				canvas.drawRect(this.fuelTankX+10, this.fuelTankY+10, this.fuelTankX+10+this.fuelLength, this.fuelTankY+this.FUEL_TANK_HEIGHT-10, this.fuelTankPaint);
			}else{//fuel has run out.
				if(SubmisionSfcView.this.bgPlayer.isPlaying())
					SubmisionSfcView.this.bgPlayer.stop();
			}//end of draw fuel level
			
			
		//	while(this.startCount<4){
				switch (this.startCount){
				case 0:
					canvas.drawBitmap(this.threeBm, this.gameOverX, this.gameOverY,null);
					this.soundPool.play(this.sourceId_3, 1, 1, 0, 0, 1);
					break;
					
				case 1:
					canvas.drawBitmap(this.twoBm, this.gameOverX, this.gameOverY,null);
					this.soundPool.play(this.sourceId_3, 1, 1, 0, 0, 1);
					break;
					
				case 2:
					canvas.drawBitmap(this.oneBm, this.gameOverX, this.gameOverY,null);
					this.soundPool.play(this.sourceId_3, 1, 1, 0, 0, 1);
					break;
					
				case 3:
					canvas.drawBitmap(this.goBm, this.gameOverX, this.gameOverY,null);
					this.soundPool.play(this.sourceId_6, 1, 1, 0, 0, 1);
					startCount=4;
					if(!SubmisionSfcView.this.bgPlayer.isPlaying())
						SubmisionSfcView.this.bgPlayer.start();
				//	this.isGameStarted = true;
					break;
				
		//		}
				
			
			}
		}//end of doDrawing()
		
	}
	
}
