package com.example.planetlander;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;


public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		this.findViewById(R.id.ControlPadBtn).setOnClickListener(this);
		this.findViewById(R.id.submissionBtnCtrlBtn).setOnClickListener(this);
		this.findViewById(R.id.terrainBtn).setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	@Override
	public void onClick(View view) {
		Intent intent = null;
		switch(view.getId()){
		case(R.id.ControlPadBtn): 
			intent =new Intent(this,ControlPadActivity.class); 
			startActivity(intent); 
			break;
		
		case (R.id.submissionBtnCtrlBtn):
			intent =new Intent(this,SubmissionActivity.class); 
									
			startActivity(intent); 
		
			break;

		case (R.id.terrainBtn):
			intent =new Intent(this,TerrainActivity.class); 
			startActivity(intent); 
			
			break;		
		}
		
		
	}

}
