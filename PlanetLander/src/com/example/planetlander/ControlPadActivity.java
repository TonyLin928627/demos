package com.example.planetlander;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;

public class ControlPadActivity extends Activity implements OnTouchListener {
	private ControlPadSfcView controlPadSfcView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_control_pad);
		
		this.findViewById(R.id.showIndicatorBtn).setOnTouchListener(this);
		this.controlPadSfcView = (ControlPadSfcView)this.findViewById(R.id.controlPadSfcView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	//	getMenuInflater().inflate(R.menu.submission, menu);
		return true;
	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		
		switch (event.getAction()){
		
		case MotionEvent.ACTION_UP:
			this.controlPadSfcView.showHelpLines(false);
			v.setBackgroundResource(R.drawable.help);
			break;
		case MotionEvent.ACTION_DOWN:		
			v.setBackgroundResource(R.drawable.help_3);
			this.controlPadSfcView.showHelpLines(true);
			break;	
		}
		return true;

	}
}
