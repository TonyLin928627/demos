package com.example.planetlander;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;


public class ControlPadSfcView extends SurfaceView implements SurfaceHolder.Callback, OnTouchListener{
	private static final float HORI_MOVE_SPEED = 2.0f;
	private static final float VERT_MOVE_SPEED = 1.5f;
	private static final int MOVE_SENSITIVITY = 50;
	
	private SurfaceHolder  holder;
	private MediaPlayer bgPlayer;
	
	private DrawThread drawThread;
	
	//spacescraft
	
	
	public ControlPadSfcView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		this.holder = this.getHolder();
		this.holder.addCallback(this);
		
		this.bgPlayer = MediaPlayer.create(this.getContext(), R.raw.bg);
		this.bgPlayer.setLooping(true);
		this.bgPlayer.setVolume(0.5f, 0.5f);
		
	//	this.findViewById(R.id.rightBtn).setVisibility(GONE);
	//	this.findViewById(R.id.upBtn).setVisibility(GONE);
	//	this.findViewById(R.id.leftBtn).setVisibility(GONE);
		
		this.setOnTouchListener(this);
		
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		
		if (this.drawThread==null){
			this.drawThread = new DrawThread(holder, 120.0f, 80.0f);
			this.drawThread.start();
			
			this.bgPlayer.start();
		}
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		
		this.bgPlayer.stop();
		
		try {
			this.drawThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		this.drawThread = null;
		
		
	}
	
	public void showHelpLines(boolean b){
		this.drawThread.setShowHelpLines(b);
	}
	
	private class DrawThread extends Thread{
		
		private SurfaceHolder surfaceHolder;
		
		private static final float MARS_G =  3.72f/3;
		private long startFallingTime;
		
		//for terrain drawing
		private float maxSpan_x;
		private float maxSpan_y;
		
		private Resources resources;
		
		//---------------------------------------
		//control pad
		private ControlPad cp;
		private float touchedX;
		private float touchedY;
		private float spacecraftX;
		private float spacecraftY;
		
		private float planetX = 40.0f;
		
		private Bitmap spacecraftBm;
		private Bitmap mainFlameBm;
		private Bitmap thrusterBm;
		private Bitmap landIndicatorBm;	
		private Bitmap explosionBm;
		private Bitmap gameOverBm;
		
		private float gameOverX;
		private float gameOverY;
		
		private final Paint aPaint = new Paint();
		private final Paint fuelTankPaint = new Paint();
		
		private final float HIGHEST_Y;
	//	private boolean isRspToTouch = true;
		
		private boolean isDrawIndicator = true;
		private boolean isShowHelpLines = false;
		
		private boolean isLanded = false;
		private boolean isCrashed = false;	
		
		private SoundPool soundPool; 
		private int sourceId_1;
		private int sourceId_2;
		
		private float[] crashPoint;
		/*
		 * 0. space sky
		 * 1. planet
		 * 2. terrain
		 * 3. direction  buttons
		 * 4. spacecraft
		 * 5. land indicator
		 * 6. control pad
		 */
		private List drawableList = new ArrayList();

		private float[][] landIndicatorCoords;

		private final int FULL_TANK_LENGTH = 400;
		private final int FUEL_TANK_HEIGHT = 50;
		private float fuelLength = FULL_TANK_LENGTH;
		private int fuelTankX;
		private int fuelTankY;
		
		
		/**
		 * @param surfaceHolder
		 * @param maxSpan_x
		 * @param maxSpan_y
		 * @param resources
		 */
		private DrawThread(SurfaceHolder surfaceHolder, float maxSpan_x,
				float maxSpan_y) {
			super();
			this.surfaceHolder = surfaceHolder;
			this.maxSpan_x = maxSpan_x;
			this.maxSpan_y = maxSpan_y;
			this.resources = ControlPadSfcView.this.getResources();
			
			
			this.cp = new ControlPad(ControlPadSfcView.this);		 
			
		    this.soundPool = new SoundPool(3, AudioManager.STREAM_SYSTEM, 5);
		    this.sourceId_1 = soundPool.load(ControlPadSfcView.this.getContext(), R.raw.explosion, 0);
		    this.sourceId_2 = soundPool.load(ControlPadSfcView.this.getContext(), R.raw.landed, 0);
		    
			/*
			 * 0. space sky
			 * 1. planet
			 * 2. terrain
			 * 3. control pad
			 * 4. spacecraft
			 * 5. control pad
			 */
			
			Canvas  aCanvas  =  this.surfaceHolder.lockCanvas();
			int canvasW = aCanvas.getWidth();
			int canvasH = aCanvas.getHeight();
			this.surfaceHolder.unlockCanvasAndPost(aCanvas); 
			
			this.spacecraftX = canvasW/2;
			
			this.fuelTankX = 165;
			this.fuelTankY = canvasH -this.FUEL_TANK_HEIGHT-18;
			
			//0.space sky
			Path bgPath = new Path();
			bgPath.moveTo(0, 0);
			bgPath.lineTo(0, canvasH);
			bgPath.lineTo(canvasW, canvasH);
			bgPath.lineTo(canvasW, 0);
			bgPath.close();
			ShapeDrawable spaceDrawable = new ShapeDrawable(new PathShape(bgPath, 1, 1));
			Bitmap spaceBm = BitmapFactory.decodeResource(this.resources, R.drawable.space);
			Shader spaceShader = new BitmapShader(spaceBm, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
			spaceDrawable.getPaint().setShader(spaceShader); 
			spaceDrawable.setBounds(0,0,1,1);
			this.drawableList.add(spaceDrawable);
			
			//1.planet
			Bitmap planetBm = BitmapFactory.decodeResource(this.resources, R.drawable.earth);
			this.drawableList.add(planetBm);
			
			//2. terrain
			this.spacecraftBm = BitmapFactory.decodeResource(this.resources, R.drawable.craftmain);
			Terrain terrain = Terrain.createTerrain(0.4f,0.2f,this.maxSpan_x,this.maxSpan_y,canvasW, canvasH,this.spacecraftBm.getWidth());
		//	Terrain terrain = Terrain.buildTerrain(0.4f,0.2f,this.maxSpan_x,this.maxSpan_y,canvasW, canvasH);
			Path terrainPath = terrain.getPath();
			ShapeDrawable terrainDrawable = new ShapeDrawable(new PathShape(terrainPath, 1, 1));
			Bitmap terrainBm = BitmapFactory.decodeResource(this.resources, R.drawable.mars);
			Shader terrainShader = new BitmapShader(terrainBm, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
			terrainDrawable.getPaint().setShader(terrainShader);     
			terrainDrawable.setBounds(0,0,1,1); 
			this.drawableList.add(terrainDrawable);
			this.HIGHEST_Y = canvasH*(1-terrain.getHighest());
			
			
			//3. control pad
			this.drawableList.add(this.cp.getShapeBm());
			
			//4. land indicator
			this.landIndicatorBm = BitmapFactory.decodeResource(this.resources, R.drawable.indicator);
			this.drawableList.add(this.landIndicatorBm);
			List<Terrain.LandPoint> landPointsList =  Terrain.buildTerrain().getLandPointsList();
			this.landIndicatorCoords = new float[landPointsList.size()][2];
			for(int i=0; i<landPointsList.size(); i++){
				Terrain.LandPoint lp = landPointsList.get(i);
				float[] landPointArray = {((lp.getStopX()-lp.getStartX())-landIndicatorBm.getWidth())/2+lp.getStartX(),
											Math.min(lp.getStartY(),lp.getStopY()),
											lp.getStartX(),
											lp.getStartY(),
											lp.getStopX(),
											lp.getStopY(),
											};
				this.landIndicatorCoords[i] = landPointArray;
				
			}
			
			//5.spacecraft
			//	this.spacecraftBm = BitmapFactory.decodeResource(this.resources, R.drawable.craftmain);
				this.mainFlameBm = BitmapFactory.decodeResource(this.resources, R.drawable.main_flame);
				this.thrusterBm = BitmapFactory.decodeResource(this.resources, R.drawable.thruster);
				this.drawableList.add(this.spacecraftBm);
				
						
				this.explosionBm = BitmapFactory.decodeResource(this.resources, R.drawable.crash);
				this.gameOverBm = BitmapFactory.decodeResource(this.resources, R.drawable.gameover);
				
				this.gameOverX = (canvasW-this.gameOverBm.getWidth())/2;
				this.gameOverY = (canvasH-this.gameOverBm.getHeight())/2;
		}


		private void setShowHelpLines(boolean isShowHelpLines) {
			this.isShowHelpLines = isShowHelpLines;
		}


		@Override
		public void run() {
		//	long curTime = System.currentTimeMillis();
		//	Log.v("GameSfcView","run() start. ");
	 
			this.startFallingTime = System.currentTimeMillis();
	        while(true){
	        	
	        	// 锁定surface，并返回到要绘图的Canvas  
		        Canvas  canvas  =  this.surfaceHolder.lockCanvas(); 
		        
		       
		        if(canvas==null) return;
		        
		        long curTime = System.currentTimeMillis();
		        
	        	this.doDrawing(canvas);  
	        	
	        	// 解锁Canvas，并渲染当前图像  
	        	this.surfaceHolder.unlockCanvasAndPost(canvas); 
//	        	
//	        	try {
//					Thread.sleep(20);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
	        }
	        
	    //    Log.v("GameSfcView","run() stop. "+(System.currentTimeMillis()-curTime));
		}


		private synchronized void doDrawing(Canvas canvas) {
			//0. draw space background.
			((ShapeDrawable)this.drawableList.get(0)).draw(canvas);
			
			//1. draw planet
			Bitmap planetBm = ((Bitmap)this.drawableList.get(1));
			canvas.drawBitmap(planetBm, this.planetX,40, null);
			if(!this.isCrashed){
				this.planetX+=0.2;
			
				if(this.planetX>canvas.getWidth()) this.planetX = (planetBm.getWidth()*-1);
			}
			
//			if(this.isCrashed==true){
//				canvas.drawBitmap(this.explosionBm,this.crashPoint[0], this.crashPoint[1], null);
//			}
			
			//2. draw terrain   
			((ShapeDrawable)this.drawableList.get(2)).draw(canvas);
			
			if(this.isCrashed==true){
				canvas.drawBitmap(this.explosionBm,this.crashPoint[0], this.crashPoint[1], null);
				canvas.drawBitmap(this.gameOverBm, this.gameOverX, this.gameOverY, null);
			//	canvas.drawBitmap(this.gameOverBm, 0, 0, null);
				return;
			}
			
			//3. draw control pad and flames maybe.
			if (this.cp.isDrawMe()){
				//draw control pad
				canvas.drawBitmap(this.cp.getShapeBm(),this.cp.getOriginX()-this.cp.getRadius(), this.cp.getOriginY()-this.cp.getRadius(), this.cp.getPaint());
				canvas.drawCircle(this.touchedX, this.touchedY, 10, this.cp.getPaint());
				//draw origin
				canvas.drawCircle(this.cp.getOriginX(), this.cp.getOriginY(), 10, this.cp.getPaint());
				boolean isDrawLine = false;
				
				final float UP_OUTTER_RANGE = this.cp.getOriginY()-(4*MOVE_SENSITIVITY);
				final float UP_INNER_RANGE = this.cp.getOriginY()-MOVE_SENSITIVITY;
				
				final float RIGHT_OUTTER_RANGE = this.cp.getOriginX()+ (3*MOVE_SENSITIVITY);
				final float RIGHT_INNER_RANGE = this.cp.getOriginX()+MOVE_SENSITIVITY;
				
				final float LEFT_OUTTER_RANGE = this.cp.getOriginX()- (3*MOVE_SENSITIVITY);
				final float LEFT_INNER_RANGE = this.cp.getOriginX()-MOVE_SENSITIVITY;
				//go up
				if(this.touchedY<UP_INNER_RANGE&&this.touchedY>UP_OUTTER_RANGE&&this.fuelLength>0){
//					this.soundPool.stop(this.sourceId_2);
//					this.soundPool.play(this.sourceId_2, 1, 1, 0, 0, 1);
					this.spacecraftY-=VERT_MOVE_SPEED;
					
					if(this.spacecraftY<=0) this.spacecraftY = 0;

					float mainFlameX = this.spacecraftX+ (this.spacecraftBm.getWidth() - this.mainFlameBm.getWidth()) / 2;
					float mainFlameY = this.spacecraftY + this.spacecraftBm.getHeight() + 2;
					canvas.drawBitmap(this.mainFlameBm, mainFlameX,mainFlameY, this.aPaint);
					isDrawLine = true;
					
					this.isLanded = false;
					
					this.fuelLength-= 0.8f;
					
					this.startFallingTime = System.currentTimeMillis();
				}else if(!this.isLanded){
					
					float deltaTime = System.currentTimeMillis() - this.startFallingTime;
					
					//change unit from millisecond to second.
					float deltaSec = deltaTime / 1000;
					
					//calculate the displacement
					float deltaY = (MARS_G * (deltaSec * deltaSec)) / 2;
					this.spacecraftY += deltaY;
					
					
					int bottomY = canvas.getHeight() - this.spacecraftBm.getHeight();
					if (this.spacecraftY >= bottomY) {
						this.spacecraftY = bottomY;
					} 
					
				}
				
				//shift to right;
				if (this.touchedX>RIGHT_INNER_RANGE&&this.touchedX<RIGHT_OUTTER_RANGE&&!this.isLanded&&this.fuelLength>0){
//					this.soundPool.stop(this.sourceId_2);
//					this.soundPool.play(this.sourceId_2, 1, 1, 0, 0, 1);
					this.spacecraftX+=HORI_MOVE_SPEED;
					
//					if (this.spacecraftX > (canvas.getWidth()-this.spacecraftBm.getWidth()))
//						this.spacecraftX = canvas.getWidth()-this.spacecraftBm.getWidth();
					if (this.spacecraftX > canvas.getWidth())
						this.spacecraftX = 0;
					
					//draw thruster
					float thrusterX = this.spacecraftX;
					float thrusterY = this.spacecraftY+this.spacecraftBm.getHeight();
					canvas.drawBitmap(this.thrusterBm, thrusterX,thrusterY, this.aPaint);
					
					isDrawLine = true;
					
					this.fuelLength-= 0.5f;
				}
				
				
				//shift to left
				else if (this.touchedX<LEFT_INNER_RANGE&&this.touchedX>LEFT_OUTTER_RANGE&&!this.isLanded&&this.fuelLength>0){
//					this.soundPool.stop(this.sourceId_2);
//					this.soundPool.play(this.sourceId_2, 1, 1, 0, 0, 1);
					this.spacecraftX-=HORI_MOVE_SPEED;
					
				//	if (this.spacecraftX<=0) this.spacecraftX = 0;
					if (this.spacecraftX<=0) this.spacecraftX = canvas.getWidth();
					
					//draw thrust
					float thrusterX = this.spacecraftX+this.spacecraftBm.getWidth()-this.thrusterBm.getWidth();
					float thrusterY = this.spacecraftY+this.spacecraftBm.getHeight();
					canvas.drawBitmap(this.thrusterBm, thrusterX,thrusterY, this.aPaint);
					
					isDrawLine = true;
					
					this.fuelLength-= 0.5f;
				}	
				
				
				//=====================================
				// if spaceY == old spaceY spax+=deltaY;
				//=====================================
				
				if (isDrawLine){
					this.aPaint.setColor(Color.GREEN);
					canvas.drawLine(this.touchedX, this.touchedY, this.cp.getOriginX(), this.cp.getOriginY(), this.cp.getPaint());
				}
			}else{
				if(!this.isLanded){
					//not touching
					float deltaTime = System.currentTimeMillis() - this.startFallingTime;
				
					//change unit from millisecond to second.
					float deltaSec = deltaTime / 1000;
				
					//calculate the displacement
					float deltaY = (MARS_G * (deltaSec * deltaSec)) / 2;
					this.spacecraftY += deltaY;
					
				
					int bottomY = canvas.getHeight() - this.spacecraftBm.getHeight();
					if (this.spacecraftY >= bottomY) {
						this.spacecraftY = bottomY;
					} 
				}

			}
			
			
			
			//4. draw land indicator
			boolean inSaveColumn = false;
			if(this.isDrawIndicator){
				for(float[] coord : this.landIndicatorCoords){	
					canvas.drawBitmap(this.landIndicatorBm, coord[0], coord[1], null);
					if(this.isShowHelpLines){
						this.aPaint.setColor(Color.WHITE);
						canvas.drawLine(coord[2], 0, coord[2], coord[3], this.aPaint);
						canvas.drawLine(coord[4], 0, coord[4], coord[5], this.aPaint);
					}
					
					if(this.spacecraftX>(coord[2]-10)&&((this.spacecraftX+this.spacecraftBm.getWidth())<(coord[4])+10)){
						inSaveColumn = true;
					}
				}
			}		
				
			//5.  draw spacecraft or crash site
			float[] touchTerrainPoint = null;
			float bottomOfCraftY  = this.spacecraftY+this.spacecraftY;
			if(bottomOfCraftY>this.HIGHEST_Y){
				touchTerrainPoint = Terrain.doTouchDetection(this.spacecraftX, this.spacecraftY, this.spacecraftBm.getWidth(),this.spacecraftBm.getHeight());
			}
			if(touchTerrainPoint==null){	//not touching land
				if(inSaveColumn&&this.isShowHelpLines){
			//	if(inSaveColumn){
//					canvas.drawRect(this.spacecraftX, 
//							this.spacecraftY, 
//							this.spacecraftX+this.spacecraftBm.getWidth(), 
//							this.spacecraftY+this.spacecraftBm.getHeight(), 
//							this.aPaint);
					this.aPaint.setAlpha(100);
					canvas.drawBitmap(this.spacecraftBm, this.spacecraftX, this.spacecraftY, this.aPaint);
					
				}else{
					this.aPaint.setAlpha(255);
					canvas.drawBitmap(this.spacecraftBm, this.spacecraftX, this.spacecraftY, this.aPaint);
				}
			}else{	// crash site or landed successfully
				float landSpeed = 0;
				if(!this.isLanded){
					landSpeed = MARS_G * (System.currentTimeMillis()-this.startFallingTime);
				}
				
				if(inSaveColumn&&landSpeed<3000){ //landed successfully
					canvas.drawBitmap(this.spacecraftBm, this.spacecraftX, this.spacecraftY, this.aPaint);
					if(! this.isLanded){
						this.soundPool.play(this.sourceId_2, 0.1f, 0.1f, 0, 0, 1);
						this.isLanded = true;
					}
				}else{ //crashes
					this.isCrashed = true;
					this.crashPoint = new float[2];
					
					this.crashPoint[0] = this.spacecraftX-((this.explosionBm.getWidth()-this.spacecraftBm.getWidth())/2);
					this.crashPoint[1] = this.spacecraftY-((this.explosionBm.getHeight()-this.spacecraftBm.getHeight())/2)+25;
					
				//	this.crashPoint[0] = touchTerrainPoint[0]-40;
				//	this.crashPoint[1] = touchTerrainPoint[1]-31.5f;
					
					
					ControlPadSfcView.this.bgPlayer.stop();
					this.soundPool.play(this.sourceId_1, 0.3f, 0.3f, 0, 0, 1);
					
				//	this.crashPoint[0] = this.spacecraftX+(this.spacecraftBm.getWidth()/2);
				//	this.crashPoint[1] = this.spacecraftY+(this.spacecraftBm.getHeight()/2);	
				//	this.aPaint.setColor(Color.BLACK);
				//	canvas.drawCircle(this.crashPoint[0], this.crashPoint[1], 75, this.aPaint);
				}	
			}
			
			// 6. draw fuelTank
			
			//draw fuel tank frame
		//	this.fuelTankPaint.setAlpha(200);
			this.fuelTankPaint.setColor(Color.GRAY);
			canvas.drawRect(this.fuelTankX, this.fuelTankY, this.fuelTankX+this.FULL_TANK_LENGTH+20, this.fuelTankY+this.FUEL_TANK_HEIGHT, this.fuelTankPaint);
			
			//draw fuel level
				if(this.fuelLength>0){
			//		this.fuelTankPaint.setAlpha(50);
					this.fuelTankPaint.setColor(Color.GREEN);
					canvas.drawRect(this.fuelTankX+10, this.fuelTankY+10, this.fuelTankX+10+this.fuelLength, this.fuelTankY+this.FUEL_TANK_HEIGHT-10, this.fuelTankPaint);
			}
		}
		
	}
	

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		
	//	if (!this.drawThread.isRspToTouch){
		if(this.drawThread.isCrashed){
			return true;
		}
		
		switch (event.getAction()){
		
		case MotionEvent.ACTION_UP:
			this.drawThread.cp.setDrawMe(false);
			
			this.drawThread.cp.setOriginX(-1);
			this.drawThread.cp.setOriginY(-1);
			break;
			
		case MotionEvent.ACTION_DOWN:					
			this.drawThread.cp.setOriginX(event.getX(0));
			this.drawThread.cp.setOriginY(event.getY(0));
			this.drawThread.cp.setDrawMe(true);
			
			break;
			
		
		case MotionEvent.ACTION_MOVE:
			this.drawThread.touchedX = event.getX(0);
			this.drawThread.touchedY = event.getY(0);
			
			break;
			
		}
		
		return true;
	}
}
