package com.example.planetlander;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class TerrainActivity extends Activity implements OnSeekBarChangeListener {
	private SeekBar stepXsBar;
	private SeekBar stepYsBar;
	private TerrainSfcView tsfcView;
	private TextView tv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_terrain);
		
		stepXsBar = (SeekBar)this.findViewById(R.id.stepXBar);
		stepXsBar.setMax(200);
		stepXsBar.setOnSeekBarChangeListener(this);
		
		stepYsBar = (SeekBar)this.findViewById(R.id.stepYBar);
		stepYsBar.setMax(100);
		stepYsBar.setOnSeekBarChangeListener(this);
		
		tsfcView = (TerrainSfcView)this.findViewById(R.id.terrainSfcView);
		
		tv = (TextView)this.findViewById(R.id.terrainSpanTv);
		tv.setText("maxSpanX="+TerrainSfcView.INIT_MAX_SPAN_X+", maxSpanY="+TerrainSfcView.INIT_MAX_SPAN_Y);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.terrain_activity1, menu);
		return true;
	}

	@Override
	public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		float spx = this.stepXsBar.getProgress()+TerrainSfcView.INIT_MAX_SPAN_X;
		float spy = this.stepYsBar.getProgress()+TerrainSfcView.INIT_MAX_SPAN_Y;
		
		tsfcView.reDraw(spx,spy);
		Log.v("TerrainActivity",this.stepXsBar.getProgress()+","+this.stepYsBar.getProgress() );
		
		tv.setText("maxSpanX="+spx+", maxSpanY="+spy);

//		switch(seekBar.getId()){
//		case R.id.stepXBar:	
//			
//			tsfcView.reDraw(this.stepXsBar.getProgress()+TerrainSfcView.INIT_MAX_SPAN_X,this.stepYsBar.getProgress()+TerrainSfcView.INIT_MAX_SPAN_Y);
//			Log.v("TerrainActivity",this.stepXsBar.getProgress()+","+this.stepYsBar.getProgress() );
//			break;
//		case R.id.stepYBar:
//			
//			tsfcView.reDraw(this.stepXsBar.getProgress()+TerrainSfcView.INIT_MAX_SPAN_X,this.stepYsBar.getProgress()+TerrainSfcView.INIT_MAX_SPAN_Y);
//			Log.v("TerrainActivity",this.stepXsBar.getProgress()+","+this.stepYsBar.getProgress() );
//			
//			break;
//			
//		}
		
	}
}
