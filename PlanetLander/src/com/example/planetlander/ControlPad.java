package com.example.planetlander;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

public class ControlPad {
	private long radius;
	
	private boolean isDrawMe;
	private Bitmap shapeBm;
	
	private Paint aPaint;
	
	private float originX;
	private float originY;
	
	
	public Bitmap getShapeBm() {
		return shapeBm;
	}
	
	public void setShapeBm(Bitmap shapeBm) {
		this.shapeBm = shapeBm;
	}
	
	
	public float getOriginX(){
		return this.originX;
	}
	
	
	public float getOriginY(){
		return this.originY;
	}
	
	
	public void setOriginX(float originX) {
		this.originX = originX;
	}

	public void setOriginY(float originY) {
		this.originY = originY;
	}

	public boolean isDrawMe() {
		return isDrawMe;
	}
	
	public void setDrawMe(boolean isDrawMe) {
		this.isDrawMe = isDrawMe;
	}
	public Bitmap getButtonShape() {
		return shapeBm;
	}
	public void setButtonShape(Bitmap padShape) {
		this.shapeBm = padShape;
	}
	
	public ControlPad(View view){
		
//		Drawable drawable = view.getResources().getDrawable(R.drawable.rect);
//		this.shapeBm = drawableToBitmap(drawable);
//		Log.v("ControlButton",this.shapeBm.getWidth()+"_1");
//		
		this.shapeBm = BitmapFactory.decodeResource(view.getResources(), R.drawable.radar);
		this.radius = (this.shapeBm.getWidth()-1)/2;
		
		Log.v("ControlButton",this.shapeBm.getWidth()+"_2");
		
		this.aPaint = new Paint();
		this.aPaint.setAntiAlias(true);
		this.aPaint.setColor(Color.RED);
		this.aPaint.setAlpha(980);
		
	}
	public long getRadius() {
		return radius;
	}

	public void setRadius(long radius) {
		this.radius = radius;
	}

	public Paint getPaint() {

		return this.aPaint;
	}
	
	public static Bitmap drawableToBitmap(Drawable drawable) {

		Bitmap bitmap = Bitmap
				.createBitmap(
						drawable.getIntrinsicWidth(),
						drawable.getIntrinsicHeight(),
						drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
								: Bitmap.Config.RGB_565);
		Canvas canvas = new Canvas(bitmap);
		// canvas.setBitmap(bitmap);
		drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),drawable.getIntrinsicHeight());
		drawable.draw(canvas);
		
		Log.v("ControlButton",bitmap.getWidth()+":"+bitmap.getHeight());
		return bitmap;
	}
}
