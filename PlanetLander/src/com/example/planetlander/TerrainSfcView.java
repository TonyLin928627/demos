package com.example.planetlander;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.graphics.Paint.Style;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class TerrainSfcView extends SurfaceView implements SurfaceHolder.Callback{
	private SurfaceHolder  holder;
	private ViewUpdateThread vuThread;
	
	public final static float INIT_MAX_SPAN_X = 20f;
	public final static float INIT_MAX_SPAN_Y = 10f;

	public TerrainSfcView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		this.holder = this.getHolder();
		this.holder.addCallback(this);
	}

		  
	//########implementations of SurfaceHolder.Callback#######
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		Log.v("TerrianView","surfaceChanged() is called");
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.v("TerrianView","surfaceCreated() is called");
	     if ( this.vuThread  == null) {
	    	 this.vuThread  =  new ViewUpdateThread(this.holder,this.getResources(),INIT_MAX_SPAN_X,INIT_MAX_SPAN_Y);
	    	 
	    	 this.vuThread.start();
		}  
	}
	     
    	 
		

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.v("TerrianView","surfaceDestroyed() is called");  
	     //kill vuThread
	     if (this.vuThread != null) {  
		    	this.vuThread  =  null ;  
		 }  
		
	}
	
	//##########################################################

	
	private class ViewUpdateThread extends Thread{
		private boolean isJobDone;
		private SurfaceHolder surfaceHolder;
		private Paint aPaint;
	//	private float x = 0;
	//	private float y = 0;
		
		private float maxSpan_x = 0f;
		private float maxSpan_y = 0f;
		
		private Resources resources;
		
		private ViewUpdateThread( SurfaceHolder  surfaceHolder, Resources resources, float maxSpan_x, float maxSpan_y){
			super();
			this.isJobDone = false;
			this.surfaceHolder = surfaceHolder;
			this.aPaint = new Paint();
			this.aPaint.setColor(Color.YELLOW);
			this.resources = resources;
			this.maxSpan_x = maxSpan_x;
			this.maxSpan_y = maxSpan_y;
		}
		
		@Override
		public void run() {
			long curTime = System.currentTimeMillis();
			Log.v("TerrianView","run() start. ");
			while (true) {  
		        // 锁定surface，并返回到要绘图的Canvas  
		        Canvas  canvas  =  this.surfaceHolder .lockCanvas();  
		 
		        // 待实现：在Canvas上绘图  
		        this.doDrawing(canvas);
		      //  canvas.drawColor(Color.BLUE);
		      //  canvas.drawCircle(100.0f, 100.0f, 50.0f, this.aPaint);
		        
		        
		 
		        // 解锁Canvas，并渲染当前图像  
		        this.surfaceHolder.unlockCanvasAndPost(canvas);  
		        
		        Log.v("TerrianView","run() stop. "+(System.currentTimeMillis()-curTime));
		        break;
//		        try {
//					Thread.sleep(100);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
			}  
		}

		private void doDrawing(Canvas canvas) {
		//	 canvas.drawCircle(x++, y++, 20, aPaint);
			canvas.drawColor(Color.BLACK);
			
			ShapeDrawable mDrawable = null;
			Bitmap bitmap = null;
			Shader aShader = null;
			
			//draw space background.
			Path bgPath = new Path();
			bgPath.moveTo(0, 0);
			bgPath.lineTo(0, canvas.getHeight());
			bgPath.lineTo(canvas.getWidth(), canvas.getHeight());
			bgPath.lineTo(canvas.getWidth(), 0);
			bgPath.close();
			mDrawable = new ShapeDrawable(new PathShape(bgPath, 1, 1));
			bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.space);
			aShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
			mDrawable.getPaint().setShader(aShader); 
			mDrawable.setBounds(0,0,1,1);  
			mDrawable.draw(canvas);
			
			//draw planet
			bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.earth);
			canvas.drawBitmap(bitmap, 40,40, null);
			
			
			//draw terrain
			Path path = Terrain.createTerrain(0.4f,0.2f,this.maxSpan_x,this.maxSpan_y,canvas.getWidth(), canvas.getHeight(), 0).getPath();
			mDrawable = new ShapeDrawable(new PathShape(path, 1, 1));
			bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.mars2);
			aShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
			mDrawable.getPaint().setShader(aShader);     
			mDrawable.setBounds(0,0,1,1);    
			mDrawable.draw(canvas);
			
		}

		private float getMaxSpan_x() {
			return maxSpan_x;
		}

		private void setMaxSpan_x(float maxSpan_x) {
			this.maxSpan_x = maxSpan_x;
		}

		private float getMaxSpan_y() {
			return maxSpan_y;
		}

		private void setMaxSpan_y(float maxSpan_y) {
			this.maxSpan_y = maxSpan_y;
		}
		
		
	}

	public void reDraw(float maxSpan_x, float maxSpan_y) {
	    this.vuThread  =  new ViewUpdateThread(this.holder,this.getResources(),INIT_MAX_SPAN_X,INIT_MAX_SPAN_Y);
		this.vuThread.setMaxSpan_x(maxSpan_x);
		this.vuThread.setMaxSpan_y(maxSpan_y);
		
		this.vuThread.start();
		
	}
	
}



