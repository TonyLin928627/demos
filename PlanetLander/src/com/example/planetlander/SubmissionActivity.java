package com.example.planetlander;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;

public class SubmissionActivity extends Activity implements OnTouchListener {
	private SubmisionSfcView submisionSfcView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_submission);
		
		ControlOnTouchListener controlOnTouchListener = new ControlOnTouchListener();
		
		this.findViewById(R.id.showIndicator_bunCtrlBtn).setOnTouchListener(this);
		
		this.findViewById(R.id.leftBtn).setOnTouchListener(controlOnTouchListener);
		this.findViewById(R.id.upBtn).setOnTouchListener(controlOnTouchListener);
		this.findViewById(R.id.rightBtn).setOnTouchListener(controlOnTouchListener);
		
		this.submisionSfcView = (SubmisionSfcView)this.findViewById(R.id.gameSfcView_btnCtrl);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.submission_activity_btn_ctrl, menu);
		return true;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		switch (event.getAction()){
		
		case MotionEvent.ACTION_UP:
			this.submisionSfcView.showHelpLines(false);
			v.setBackgroundResource(R.drawable.help);
			break;
		case MotionEvent.ACTION_DOWN:		
			v.setBackgroundResource(R.drawable.help_3);
			this.submisionSfcView.showHelpLines(true);
			break;	
		}
		return true;
	}

	private class ControlOnTouchListener implements OnTouchListener{

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			switch (event.getAction()){
			
			case MotionEvent.ACTION_DOWN:
				v.setAlpha(250);
				switch(v.getId()){
				case R.id.leftBtn: 
					SubmissionActivity.this.submisionSfcView.leftThrusterOn(true);
					v.setBackgroundResource(R.drawable.pressed_btn);
					break;
				
				case R.id.rightBtn:
					SubmissionActivity.this.submisionSfcView.rightThrusterOn(true);
					v.setBackgroundResource(R.drawable.pressed_btn);
					break;
				
				case R.id.upBtn:
					SubmissionActivity.this.submisionSfcView.mainThrusterOn(true);
					v.setBackgroundResource(R.drawable.pressed_btn);
					break;
				}
				break;
			case MotionEvent.ACTION_UP:		
				v.setAlpha(100);
				switch(v.getId()){
				case R.id.leftBtn: 
					SubmissionActivity.this.submisionSfcView.leftThrusterOn(false);
					v.setBackgroundResource(R.drawable.left_btn);

					break;
			
				case R.id.rightBtn:
					SubmissionActivity.this.submisionSfcView.rightThrusterOn(false);
					v.setBackgroundResource(R.drawable.right_btn);
					break;
			
				case R.id.upBtn:
					SubmissionActivity.this.submisionSfcView.mainThrusterOn(false);
					v.setBackgroundResource(R.drawable.main_btn);
					break;
				}
			}
			return true;
		}
		
	}
}

